﻿using System.Reflection;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("Suporte")]
[assembly: AssemblyDescription("Projeto destinado para gerar DLL de suporte para projetos bases do DER, tanto como ASP.NET e MVC")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("DER")]
[assembly: AssemblyProduct("Suporte.ddl desenvolvido por Luis Padovani")]
[assembly: AssemblyCopyright("Copyright ©  2017")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("pt-br")]
 

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("7aa59bf9-d796-4ad7-906d-2ef589dd3fc6")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// 1.0.0.2 - Adicionado metodo de mensagem Banco de Dados
// 1.0.0.3 - Adicionado metodo de mensagem para MVC
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("1.0.0.3")]
[assembly: AssemblyFileVersion("1.0.0.3")]
