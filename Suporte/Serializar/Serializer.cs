﻿using System;
using System.IO;
using System.Xml.Serialization;
using Newtonsoft.Json;
using Suporte.Exceptions;
using Suporte.MsgBox;
using Suporte.MsgBox.AspNet;

namespace Suporte.Serializar
{
    public class Serializer<T> 
    {

        public static string ConvertToJson(object obj)
        {
            try
            {
                return JsonConvert.SerializeObject(obj);
            }
            catch (Exception)
            {
                throw new ArgumentException("Erro na conversão de objeto para json.");
            }
        }

        public static T ConvertFromJson(string json)
        {
            try
            {
                return JsonConvert.DeserializeObject<T>(json);
            }
            catch (Exception)
            {
                throw new ArgumentException("Erro na conversão de json para objeto.");
            }
        }
        
        public string ToXml(T o)
        {

            try
            {
                var serializer = new XmlSerializer(typeof(T));

                using (var stringWriter = new StringWriter())
                {
                    serializer.Serialize(stringWriter, o);
                    return stringWriter.ToString();
                }
            }
            catch (Exception)
            {
                throw new CustomException("Erro na conversão para XML.", bootStrapMessageType.TYPE_DANGER);  
            }
        }

        public T FromXml(string xml)
        {
            try
            {
                var serializer = new XmlSerializer(typeof(T));
                using (var stringReader = new StringReader(xml))
                {
                    return (T)serializer.Deserialize(stringReader);
                }
            }
            catch (Exception)
            {
                throw new CustomException("Erro na conversão do XML.", bootStrapMessageType.TYPE_DANGER);   
            }
        }
    }
}
