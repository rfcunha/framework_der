﻿using System;
using System.Collections;
using System.IO;
using Suporte.Exceptions;
using Suporte.MsgBox;
using Suporte.MsgBox.AspNet;
using Tamir.SharpSsh;

namespace Suporte.Ftp
{
    public class STFP
    {
        private string host;
        private string user;
        private string pass;
        private int port = 21;

        public STFP(string hostIP, string userName, string password)
        {
            host = hostIP;
            user = userName;
            pass = password;
        }

        public STFP(string hostIP, string userName, string password, int porta)
        {
            host = hostIP;
            user = userName;
            pass = password;
            port = porta;
        }

        /// <summary>
        /// RETORNO LISTA DE ARQUIVOS DENTRO DO DIRETORIO
        /// </summary>
        /// <param name="directory"></param>
        /// <returns></returns>
        public ArrayList getFilesDirectory(string directory)
        {
            try
            {
                var client = new Sftp(host, user, pass);

                client.Connect();

                return client.GetFileList(directory);
                
            }
            catch (CustomException ce)
            {
                throw ce;
            }
            catch (Exception)
            {
                throw new CustomException("Ocorreu um erro ao listar os arquivos. <br/> Entre em contato com suporte.", bootStrapMessageType.TYPE_DANGER);
            }
        }

        /// <summary>
        /// Metodo responsavel pelo download arquivo do STFP
        /// </summary>
        /// <param name="ftpFilePath"></param>
        /// <param name="destinationFile"></param>
        public void DownloadFile(string ftpFilePath, string destinationFile)
        {
            try
            {
                var client = new Sftp(host.Replace("ftp://", "").Replace(":22", ""), user, pass);

                client.Connect(port);

                client.Get(ftpFilePath, destinationFile);

            }
            catch (CustomException ce)
            {
                throw ce;
            }
            catch (Exception)
            {
                throw new CustomException("Ocorreu um erro ao listar os arquivos. <br/> Entre em contato com suporte.", bootStrapMessageType.TYPE_DANGER);
            }
        }

        /// <summary>
        /// Metodo responsavel de realizar o upload arquivo do STFP
        /// </summary>
        /// <param name="destinationFolder"></param>
        /// <param name="fileInf"></param>
        public void UploadFile(string destinationFolder, FileInfo fileInf)
        {
            try
            {
                var client = new Sftp(host.Replace("ftp://", "").Replace(":22", ""), user, pass);

                client.Connect(port);

                // upload the 'test.zip' file to the current directory at the server 
                client.Put(fileInf.FullName, destinationFolder + fileInf.Name);

                client.Close();

                //client.Put()

            }
            catch (CustomException)
            {
                throw;
            }
            catch (Exception)
            {
                throw new CustomException("Ocorreu um erro ao realizar o upload do arquivo no ftp. <br/> Entre em contato com suporte.", bootStrapMessageType.TYPE_DANGER);
            }
        }
    }
}
