﻿using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Text;

namespace Suporte.Imagem
{
    public class Imagem
    {
        /// <summary>
        /// Converter imagem para Byte
        /// </summary>
        /// <param name="imageIn">A imagem</param>
        /// <param name="type">Tipo de imagem</param>
        /// <returns>Retorna byte da imagem</returns>
        public byte[] imageToByteArray(Image imageIn, ImageFormat type)
        {
            var ms = new MemoryStream();
            imageIn.Save(ms, type);
            return ms.ToArray();
        }

        /// <summary>
        /// Converter Byte para Imagem
        /// </summary>
        /// <param name="byte[]">Byte da imagem</param> 
        /// <returns>Retorna a Imagem</returns>
        public Image byteArrayToImage(byte[] byteArrayIn)
        {
            var ms = new MemoryStream(byteArrayIn);
            var returnImage = Image.FromStream(ms);
            return returnImage;
        }

        public Image stringToImage(string pathImagem)
        {
            return Image.FromFile(pathImagem); 
        }
    }
}
