﻿using System;
using System.Collections;
using System.Text;
using System.Web;
using System.Web.UI;
using Suporte.Exceptions;

namespace Suporte.MsgBox.AspNet
{
    public class bootStrapDialog
    {

        //private static bootStrapMessageType ConvertTypeBootStrap(MessageBox.MessageType type)
        //{
        //    var retorno = bootStrapMessageType.TYPE_DEFAULT;

        //    switch (type)
        //    {
        //        case MessageBox.MessageType.error:
        //            retorno = bootStrapMessageType.TYPE_DANGER;
        //            break;

        //        case MessageBox.MessageType.success:
        //            retorno = bootStrapMessageType.TYPE_SUCCESS;
        //            break;

        //        case MessageBox.MessageType.alert:
        //            retorno = bootStrapMessageType.TYPE_WARNING;
        //            break;

        //        case MessageBox.MessageType.info:
        //            retorno = bootStrapMessageType.TYPE_INFO;
        //            break;
        //    }

        //    return retorno;
        //}

        private readonly bootStrapMessageType _tipoErro;

        public bootStrapMessageType GetTipoErroMsgBox
        {
            get { return _tipoErro; }
        }

        private static Hashtable handlerPages = new Hashtable();

        //Classe com os dados que serão armazenados das mensagens
        private class MessageBoxItem
        {
            public string Title;
            public string Message;
            public bootStrapMessageType Type;
            public string UrlRedirect;
            public bool NewInsert;

            public MessageBoxItem(string titulo, string message, bootStrapMessageType type)
            {
                Title = titulo;
                Message = message;
                Type = type;
            }

            public MessageBoxItem(string titulo, string message, bootStrapMessageType type, string redirecionamento, bool novoCadastro)
            {
                Title = titulo;
                Message = message;
                Type = type;
                UrlRedirect = redirecionamento;
                NewInsert = novoCadastro;
            }

            public MessageBoxItem(CustomException c)
            {
                Title = c.GetTitulo;
                Message = c.Message;
                //Type = ConvertTypeBootStrap(c.GetTipoErroMsgBox);
                Type = c.GetTipoErroBootStrapDialog;
            }
        }

        //Enum com os possíveis tipos de mensagens

        /// <summary>
        /// EXIBE BOOTSTRAP DIALOG MESSAGE
        /// </summary>
        /// <param name="title"></param>
        /// <param name="message"></param>
        /// <param name="type"></param>
        public static void Show(string title, string message, bootStrapMessageType type)
        {
            if (!(handlerPages.Contains(HttpContext.Current.Handler)))
            {
                var currentPage = (Page)HttpContext.Current.Handler;

                //Include(currentPage);

                if (!((currentPage == null)))
                {
                    var queue = new Queue();
                    queue.Enqueue(new MessageBoxItem(title, message, type));
                    handlerPages.Add(HttpContext.Current.Handler, queue);
                    currentPage.PreRenderComplete += new EventHandler(CurrentPagePreRender);
                }
            }
            else
            {
                var queue = ((Queue)(handlerPages[HttpContext.Current.Handler]));
                queue.Enqueue(new MessageBoxItem(title, message, type));
            }
        }

        /// <summary>
        /// EXIBE BOOTSTRAP DIALOG MESSAGE
        /// </summary>
        /// <param name="title"></param>
        /// <param name="message"></param>
        /// <param name="type"></param>
        /// <param name="urlRedirect"></param>
        /// <param name="newInsert"></param>
        public static void Show(string title, string message, bootStrapMessageType type, string urlRedirect, bool newInsert)
        {
            if (!(handlerPages.Contains(HttpContext.Current.Handler)))
            {
                var currentPage = (Page)HttpContext.Current.Handler;

                //Include(currentPage);

                if (!((currentPage == null)))
                {
                    var queue = new Queue();
                    queue.Enqueue(new MessageBoxItem(title, message, type,urlRedirect,newInsert));
                    handlerPages.Add(HttpContext.Current.Handler, queue);
                    currentPage.PreRenderComplete += new EventHandler(CurrentPagePreRenderNovoCadastro);
                }
            }
            else
            {
                var queue = ((Queue)(handlerPages[HttpContext.Current.Handler]));
                queue.Enqueue(new MessageBoxItem(title, message, type,urlRedirect,newInsert));
            }
        }

        public static void Show(CustomException ce)
        {
            //Show(ce.GetTitulo, ce.Message, ConvertTypeBootStrap(ce.GetTipoErroMsgBox));
            Show(ce.GetTitulo, ce.Message, ce.GetTipoErroBootStrapDialog);
        }

        private static void CurrentPagePreRender(object sender, EventArgs e)
        {
            var currentPage = (Page)HttpContext.Current.Handler;

            var queue = ((Queue)(handlerPages[HttpContext.Current.Handler]));

            if (queue != null)
            {
                String script = "";

                while (queue.Count > 0)
                {
                    var iMsg = (MessageBoxItem)queue.Dequeue();

                    script += "BootstrapDialog.show({";
                    script += "closable: false,";
                    script += "title: '<b>' + '" + iMsg.Title + "' + '</b>',";
                    script += "message: '" + EncodeJsString(iMsg.Message) + "',";
                    script += "type: BootstrapDialog." + iMsg.Type + ",";
                    script += "buttons: [{";
                    script += "id: 'btn-ok', ";
                    script += "icon: 'glyphicon glyphicon-check',";
                    script += "label: 'OK',";
                    script += "cssClass: 'btn btn-default active',";
                    script += "autospin: false,";
                    script += "action: function(dialogRef){";
                    script += "dialogRef.close();";
                    script += "}";
                    script += "}]";
                    script += "});";

                }

                ScriptManager.RegisterStartupScript(currentPage, currentPage.GetType(), Guid.NewGuid().ToString(), script, true);
            }
        }

        private static void CurrentPagePreRenderNovoCadastro(object sender, EventArgs e)
        {
            var currentPage = (Page)HttpContext.Current.Handler;

            var queue = ((Queue)(handlerPages[HttpContext.Current.Handler]));

            if (queue != null)
            {
                String script = "";

                while (queue.Count > 0)
                {
                    var iMsg = (MessageBoxItem)queue.Dequeue();

                    if (!iMsg.NewInsert)
                    {
                        script += " BootstrapDialog.show({";
                        script += "closable: false,";
                        script += "title: '<b>' + '" + iMsg.Title + "' + '</b>',";
                        script += "message: '" + EncodeJsString(iMsg.Message) + "',";
                        script += "type: BootstrapDialog." + iMsg.Type + ",";
                        script += "buttons: [{";
                        script += "id: 'btn-ok', ";
                        script += "icon: 'glyphicon glyphicon-check',";
                        script += "label: 'OK',";
                        script += "cssClass: 'btn btn-default active',";
                        script += "autospin: false,";
                        script += "action: function(){";
                        script += !String.IsNullOrEmpty(iMsg.UrlRedirect) ? "window.location.href = '" + iMsg.UrlRedirect + "'" : string.Empty;
                        script += "}";
                        script += "}]";
                        script += "});";
                    }
                    else
                    {
                        script += "BootstrapDialog.show({";
                        script += "closable: false,";
                        script += "title: '<b>' + '" + iMsg.Title + "' + '</b>',";
                        script += "message: '" + EncodeJsString(iMsg.Message) + "',";
                        script += "type: BootstrapDialog." + iMsg.Type + ",";
                        script += "buttons: [{";
                        script += "id: 'btnNovo',";
                        script += "label: 'Novo Cadastro',";
                        script += "cssClass: 'btn btn-default active',";
                        script += "action: function(dialog) {";
                        script += " dialog.close();";
                        script += "}";
                        script += "},";
                        script += "{";
                        script += "label: 'Ok',";
                        script += "cssClass: 'btn btn-default active',";
                        script += "autospin: false,";
                        script += "action: function(){";
                        script += !String.IsNullOrEmpty(iMsg.UrlRedirect) ? "window.location.href = '" + iMsg.UrlRedirect + "'" : string.Empty; ;
                        script += "}";
                        script += "}]";
                        script += "});";
                    }

                }

                ScriptManager.RegisterStartupScript(currentPage, currentPage.GetType(), Guid.NewGuid().ToString(), script, true);
            }
        }


        /// <summary>
        /// Encodes a string to be represented as a string literal. The format
        /// is essentially a JSON string.
        /// 
        /// The string returned includes outer quotes 
        /// Example Output: "Hello \"Rick\"!\r\nRock on"
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        /// 
        private static string EncodeJsString(string s)
        {
            var sb = new StringBuilder();

            foreach (char c in s)
            {
                switch (c)
                {
                    case '\"':
                        sb.Append("\\\"");
                        break;
                    case '\'':
                        sb.Append("\\'");
                        break;
                    case '\\':
                        sb.Append("\\\\");
                        break;
                    case '\b':
                        sb.Append("\\b");
                        break;
                    case '\f':
                        sb.Append("\\f");
                        break;
                    case '\n':
                        sb.Append("\\n");
                        break;
                    case '\r':
                        sb.Append("\\r");
                        break;
                    case '\t':
                        sb.Append("\\t");
                        break;
                    default:
                        int i = (int)c;
                        if (i < 32 || i > 127)
                        {
                            sb.AppendFormat("\\u{0:X04}", i);
                        }
                        else
                        {
                            sb.Append(c);
                        }
                        break;
                }
            }

            return sb.ToString();
        }
    }
}
