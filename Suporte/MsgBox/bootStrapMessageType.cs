﻿namespace Suporte.MsgBox
{
    public enum bootStrapMessageType
    {
        TYPE_DEFAULT,
        TYPE_INFO,
        TYPE_PRIMARY,
        TYPE_SUCCESS,
        TYPE_WARNING,
        TYPE_DANGER
    }
}
