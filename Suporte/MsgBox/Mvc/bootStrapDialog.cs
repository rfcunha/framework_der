﻿using System;
using System.Text;
using System.Web.Mvc;
using Suporte.Exceptions;

namespace Suporte.MsgBox.Mvc
{
    public class bootStrapDialog : Controller
    { 
        public ActionResult Show(string titulo, string message, bootStrapMessageType type)
        {
            var script = "";
            script += "BootstrapDialog.show({";
            script += "closable: false,";
            script += "title: '<b>' + '" + titulo + "' + '</b>',";
            script += "message: '" + EncodeJsString(message) + "',";
            script += "type: BootstrapDialog." + type + ",";
            script += "buttons: [{";
            script += "id: 'btn-ok', ";
            script += "icon: 'glyphicon glyphicon-check',";
            script += "label: 'OK',";
            script += "cssClass: 'btn btn-default active',";
            script += "autospin: false,";
            script += "action: function(dialogRef){";
            script += "dialogRef.close();";
            script += "}";
            script += "}]";
            script += "});";
            return JavaScript(script);
        }

        public ActionResult Show(string titulo, string mensagem, string redirecionamento, bootStrapMessageType type, bool novoCadastro)
        {
            var script = "";
            if (!novoCadastro)
            {
                script += " BootstrapDialog.show({";
                script += "closable: false,";
                script += "title: '<b>' + '" + titulo + "' + '</b>',";
                script += "message: '" + EncodeJsString(mensagem) + "',";
                script += "type: BootstrapDialog." + type + ",";
                script += "buttons: [{";
                script += "id: 'btn-ok', ";
                script += "icon: 'glyphicon glyphicon-check',";
                script += "label: 'OK',";
                script += "cssClass: 'btn btn-default active',";
                script += "autospin: false,";
                script += "action: function(){";
                script += "window.location.href = '" + redirecionamento + "'";
                script += "}";
                script += "}]";
                script += "});";
            }
            else
            {
                script += "BootstrapDialog.show({";
                script += "closable: false,";
                script += "title: '<b>' + '" + titulo + "' + '</b>',";
                script += "message: '" + EncodeJsString(mensagem) + "',";
                script += "type: BootstrapDialog." + type + ",";
                script += "buttons: [{";
                script += "id: 'btnNovo',";
                script += "label: 'Novo Cadastro',";
                script += "cssClass: 'btn btn-default active',";
                script += "action: function(dialog) {";
                script += " dialog.close();";
                script += "}";
                script += "},";
                script += "{";
                script += "label: 'Ok',";
                script += "cssClass: 'btn btn-default active',";
                script += "autospin: false,";
                script += "action: function(){";
                script += "window.location.href = '" + redirecionamento + "'";
                script += "}";
                script += "}]";
                script += "});";
            }



            return JavaScript(script);
        }

        public ActionResult Show(string titulo, string message, string function, bootStrapMessageType type)
        {
            var script = "";
            script += "BootstrapDialog.show({";
            script += "title: '<b>' + '" + titulo + "' + '</b>',";
            script += "message: '" + EncodeJsString(message) + "',";
            script += "type: BootstrapDialog." + type + ",";
            script += "buttons: [{";
            script += "id: 'btn-ok', ";
            script += "icon: 'glyphicon glyphicon-check',";
            script += "label: 'OK',";
            script += "action:" + function;
            script += "}]";
            script += "});";
            return JavaScript(script);

        }

        public ActionResult Show(CustomException ce)
        {
            return Show(ce.GetTitulo, ce.Message.Replace("'", "\""), ce.GetTipoErroBootStrapDialog);
        }

        public ActionResult Show(CustomException ce, string function)
        {
            return Show(ce.GetTitulo, ce.Message.Replace("'", "\""), function, ce.GetTipoErroBootStrapDialog);
        }

        private static string EncodeJsString(string s)
        {
            var sb = new StringBuilder();

            foreach (char c in s)
            {
                switch (c)
                {
                    case '\"':
                        sb.Append("\\\"");
                        break;
                    case '\'':
                        sb.Append("\\'");
                        break;
                    case '\\':
                        sb.Append("\\\\");
                        break;
                    case '\b':
                        sb.Append("\\b");
                        break;
                    case '\f':
                        sb.Append("\\f");
                        break;
                    case '\n':
                        sb.Append("\\n");
                        break;
                    case '\r':
                        sb.Append("\\r");
                        break;
                    case '\t':
                        sb.Append("\\t");
                        break;
                    default:
                        int i = (int)c;
                        if (i < 32 || i > 127)
                        {
                            sb.AppendFormat("\\u{0:X04}", i);
                        }
                        else
                        {
                            sb.Append(c);
                        }
                        break;
                }
            }

            return sb.ToString();
        }
    }
}