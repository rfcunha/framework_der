﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Mail;
using System.Text;
using Suporte.Exceptions;
using Suporte.MsgBox;

namespace Suporte.Email
{
    public class Email
    {
        private readonly bool _enviarEmail;
        private readonly string _smtp;
        private readonly string _autenticacaoEmail;
        private readonly string _autenticacaoSenha;
        private readonly int _porta;
        private readonly bool _ssl;

        public Email(bool enviarEmail, string autenticacaoEmail, string autenticacaoSenha, string smtp, int porta, bool ssl)
        {
            _enviarEmail       = enviarEmail;
            _smtp              = smtp;
            _autenticacaoEmail = autenticacaoEmail;
            _autenticacaoSenha = autenticacaoSenha;
            _porta             = porta;
            _ssl               = ssl;
        }

        /// <summary>
        /// ENVIAR EMAIL
        /// </summary>
        /// <param name="emailRemetente"></param>
        /// <param name="nomeRemetente"></param>
        /// <param name="emailDestinatario"></param>
        /// <param name="assunto"></param>
        /// <param name="mensagem"></param>
        public void Enviar(string emailRemetente, string nomeRemetente, string emailDestinatario, string assunto, StringBuilder mensagem)
        {
            try
            {
                if (_enviarEmail)
                {
                    var message = new MailMessage();

                    var smtpClient = new SmtpClient(_smtp);

                    smtpClient.Credentials  = new NetworkCredential(_autenticacaoEmail, _autenticacaoSenha);
                    smtpClient.Port         = _porta;
                    smtpClient.EnableSsl    = _ssl;


                    message.IsBodyHtml = true;
                    message.From = new MailAddress(emailRemetente, nomeRemetente);
                    message.To.Add(new MailAddress(emailDestinatario.TrimStart().TrimEnd()));

                    message.Subject = assunto;

                    message.Body = mensagem.ToString();

                    smtpClient.Host = _smtp;
                    smtpClient.Send(message);

                    message.To.Clear();
                }
                else
                {
                    throw new CustomException("Opção de envio de email está desativado. Entre em contato com o suporte.",bootStrapMessageType.TYPE_INFO);
                }
            }
            catch (CustomException)
            {
                throw;
            }
            catch (Exception)
            {
                throw new CustomException("Ocorreu um erro ao enviar o email.</br>Entre em contato com  o suporte.", bootStrapMessageType.TYPE_DANGER);
            }
        }


        /// <summary>
        /// ENVIAR EMAIL
        /// </summary>
        /// <param name="emailRemetente"></param>
        /// <param name="nomeRemetente"></param>
        /// <param name="emailDestinatarios"></param>
        /// <param name="assunto"></param>
        /// <param name="mensagem"></param>
        public void Enviar(string emailRemetente, string nomeRemetente, IEnumerable<string> emailDestinatarios, string assunto, StringBuilder mensagem)
        {
            try
            {
                if (_enviarEmail)
                {
                    foreach (var destinatario in emailDestinatarios)
                    {
                        var message = new MailMessage();

                    var smtpClient = new SmtpClient(_smtp);

                    smtpClient.Credentials  = new NetworkCredential(_autenticacaoEmail, _autenticacaoSenha);
                    smtpClient.Port         = _porta;
                    smtpClient.EnableSsl    = _ssl;


                        message.IsBodyHtml = true;
                        message.From = new MailAddress(emailRemetente, nomeRemetente);
                        message.To.Add(new MailAddress(destinatario.TrimStart().TrimEnd()));

                        message.Subject = assunto;

                        message.Body = mensagem.ToString();

                        smtpClient.Host = _smtp;
                        smtpClient.Send(message);

                        message.To.Clear();
                    }
                }
                else
                {
                    throw new CustomException("Opção de envio de email está desativado. Entre em contato com o suporte.", bootStrapMessageType.TYPE_INFO);
                }
            }
            catch (CustomException)
            {
                throw;
            }
            catch (Exception)
            {
                throw new CustomException("Ocorreu um erro ao enviar o email.</br>Entre em contato com  o suporte.", bootStrapMessageType.TYPE_DANGER);
            }
        }


        /// <summary>
        /// ENVIAR EMAIL COM ANEXO
        /// </summary>
        /// <param name="emailRemetente"></param>
        /// <param name="nomeRemetente"></param>
        /// <param name="emailDestinatario"></param>
        /// <param name="assunto"></param>
        /// <param name="mensagem"></param>
        /// <param name="anexo">Caminho fisico do arquivo</param>
        public void Enviar(string emailRemetente, string nomeRemetente, string emailDestinatario, string assunto, StringBuilder mensagem, string anexo)
        {
            try
            {
                if (_enviarEmail)
                {
                    var message = new MailMessage();

                    var smtpClient = new SmtpClient(_smtp);

                    smtpClient.Credentials  = new NetworkCredential(_autenticacaoEmail, _autenticacaoSenha);
                    smtpClient.Port         = _porta;
                    smtpClient.EnableSsl    = _ssl;


                    message.IsBodyHtml = true;
                    message.From = new MailAddress(emailRemetente, nomeRemetente);
                    message.To.Add(new MailAddress(emailDestinatario.TrimStart().TrimEnd()));

                    message.Subject = assunto;

                    message.Body = mensagem.ToString();
                    message.Attachments.Add(new Attachment(anexo));

                    smtpClient.Host = _smtp;
                    smtpClient.Send(message);

                    message.To.Clear();
                }
                else
                {
                    throw new CustomException("Opção de envio de email está desativado. Entre em contato com o suporte.", bootStrapMessageType.TYPE_INFO);
                }
            }
            catch (CustomException)
            {
                throw;
            }
            catch (Exception)
            {
                throw new CustomException("Ocorreu um erro ao enviar o email.</br>Entre em contato com  o suporte.", bootStrapMessageType.TYPE_DANGER);
            }
        }


        /// <summary>
        /// ENVIAR EMAIL COM ANEXO
        /// </summary>
        /// <param name="emailRemetente"></param>
        /// <param name="nomeRemetente"></param>
        /// <param name="emailDestinatarios"></param>
        /// <param name="assunto"></param>
        /// <param name="mensagem"></param>
        /// <param name="anexo">Passar caminho fisico do arquivo</param>
        public void Enviar(string emailRemetente, string nomeRemetente, IEnumerable<string> emailDestinatarios, string assunto, StringBuilder mensagem, string anexo)
        {
            try
            {
                if (_enviarEmail)
                {
                    foreach (var destinatario in emailDestinatarios)
                    {
                        var message = new MailMessage();

                    var smtpClient = new SmtpClient(_smtp);

                    smtpClient.Credentials  = new NetworkCredential(_autenticacaoEmail, _autenticacaoSenha);
                    smtpClient.Port         = _porta;
                    smtpClient.EnableSsl    = _ssl;


                        message.IsBodyHtml = true;
                        message.From = new MailAddress(emailRemetente, nomeRemetente);
                        message.To.Add(new MailAddress(destinatario.TrimStart().TrimEnd()));

                        message.Subject = assunto;

                        message.Body = mensagem.ToString();
                        message.Attachments.Add(new Attachment(anexo));

                        smtpClient.Host = _smtp;
                        smtpClient.Send(message);

                        message.To.Clear();
                    }
                }
                else
                {
                    throw new CustomException("Opção de envio de email está desativado. Entre em contato com o suporte.", bootStrapMessageType.TYPE_INFO);
                }
            }
            catch (CustomException)
            {
                throw;
            }
            catch (Exception)
            {
                throw new CustomException("Ocorreu um erro ao enviar o email.</br>Entre em contato com  o suporte.", bootStrapMessageType.TYPE_DANGER);
            }
        }
    }
}
