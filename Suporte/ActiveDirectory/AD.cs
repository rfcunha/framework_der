﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.DirectoryServices;
using System.DirectoryServices.AccountManagement;
using System.Linq;

namespace Suporte.ActiveDirectory
{
    public static class AD
    { 
        private const string _canonicalName = "OU=Usuarios,DC=der,DC=sp";
        private const string _dominio = "der.sp";

        public static IEnumerable<UsersAd> GetDataSource(string filter = "")
        {

            string domainAndUsername = (ConfigurationManager.AppSettings["ServidorAD"] ?? "LDAP://der.sp/") + "OU=Usuarios,DC=der,DC=sp";

            var rootEntry = new DirectoryEntry(domainAndUsername);

            var dsFindOUs = new DirectorySearcher(rootEntry, "(&(&(objectClass=user)(objectCategory=person))" + filter + ")") { SearchScope = SearchScope.Base | SearchScope.Subtree };
            dsFindOUs.PropertiesToLoad.Add("cn");
            dsFindOUs.PropertiesToLoad.Add("sAMAccountName");
            dsFindOUs.PropertiesToLoad.Add("userAccountControl");
            dsFindOUs.PropertiesToLoad.Add("distinguishedName");
            dsFindOUs.PropertiesToLoad.Add("physicalDeliveryOfficeName");
            dsFindOUs.PropertiesToLoad.Add("department");
            dsFindOUs.PropertiesToLoad.Add("telephoneNumber");
            dsFindOUs.PropertiesToLoad.Add("streetAddress");
            dsFindOUs.PropertiesToLoad.Add("l");
            dsFindOUs.PropertiesToLoad.Add("st");
            dsFindOUs.PropertiesToLoad.Add("postalCode");
            dsFindOUs.PropertiesToLoad.Add("homePhone");

            dsFindOUs.PageSize = 10000;
            dsFindOUs.Sort.PropertyName = "sAMAccountName";

            foreach (SearchResult result in dsFindOUs.FindAll().Cast<SearchResult>().Where(t => t.Properties["userAccountControl"][0].ToString() == "512" || t.Properties["userAccountControl"][0].ToString() == "544" ||
                    t.Properties["userAccountControl"][0].ToString() == "66048"))
            {
                // 512 - Conta Ativa
                // 544 - Conta Habilitado, esperando primeiro Login
                // 66048 - Conta Ativa, com senha que nunca expira.
                // Ref: http://robsonalves.net/portal/ldap-useraccountcontrol-values/


                var tUsersAd = new UsersAd
                {
                    CanonicalName = GetPropertyValueFrom(result.Properties["cn"]),
                    sAMAccountName = GetPropertyValueFrom(result.Properties["sAMAccountName"]),
                    DistinguishedName = GetPropertyValueFrom(result.Properties["distinguishedName"]),
                    Sala = GetPropertyValueFrom(result.Properties["physicalDeliveryOfficeName"]),
                    Departamento = string.Format("{0}-{1}", GetPropertyValueFrom(result.Properties["l"]), GetPropertyValueFrom(result.Properties["postalCode"])),
                    Telefone = GetPropertyValueFrom(result.Properties["telephoneNumber"]),
                    Ramal = GetPropertyValueFrom(result.Properties["homePhone"]),
                    Coordenadoria = GetPropertyValueFrom(result.Properties["streetAddress"])
                };
                yield return tUsersAd;


            }
        }

        public static string GetPropertyValueFrom(ResultPropertyValueCollection property)
        {
            try
            {
                return property[0].ToString();
            }
            catch (Exception)
            {
                return string.Empty;
            }

        }

        /// <summary>
        /// Retorna o usuario
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static UsersAd GetUser(string value)
        {
            return GetDataSource("(&(SAMAccountName=" + value + "))").FirstOrDefault();
        }

        /// <summary>
        /// Retorna a lista de usuários referente ao valor passado
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static IEnumerable GetUsers(string value)
        {
            var filtro = "(|(SAMAccountName=*" + value + "*)(cn=*" + value + "*))";


            return GetDataSource(filtro).ToList();
        }

        /// <summary>
        /// Retorna a lista de usuários referente ao valor passado
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static IEnumerable GetNames(string value)
        {
            var filtro = "(|(SAMAccountName=*" + value + "*)(cn=*" + value + "*))";
            return GetDataSource(filtro).Select(u =>
                new
                {
                    label = u.sAMAccountName.ToUpper() + "-" + u.CanonicalName.ToUpper(),
                    value = u.CanonicalName.ToUpper(),
                    CanonicalName = u.CanonicalName.CamelCase(),
                    sAMAccountName = u.sAMAccountName.ToLower(),
                    u.UserAccountControl,
                    u.DistinguishedName,
                    u.Sala,
                    u.Departamento,
                    u.Ramal
                }).ToList();
        }

        /// <summary>
        /// Validação do usuario se o mesmo existe no AD
        /// </summary>
        /// <param name="usu_login"></param>
        /// <returns>Boleano</returns>
        public static bool? ValidateUser(string usu_login)
        {
            var oPrincipalContext = new PrincipalContext(ContextType.Domain, _dominio, _canonicalName, ContextOptions.Negotiate, ConfigurationManager.AppSettings["UserId"], ConfigurationManager.AppSettings["Password"]);
            var oUserPrincipal = UserPrincipal.FindByIdentity(oPrincipalContext, usu_login);
            return oUserPrincipal != null ? oUserPrincipal.Enabled : false;
        }

        /// <summary>
        /// Valida Credencial
        /// </summary>
        /// <param name="usu_nome"></param>
        /// <param name="usu_password"></param>
        /// <returns></returns>
        public static bool? ValidateCredentials(string usu_nome, string usu_password)
        {
            var userPrincipal = new PrincipalContext(ContextType.Domain, _dominio, _canonicalName, ConfigurationManager.AppSettings["UserId"], ConfigurationManager.AppSettings["Password"]);
            return userPrincipal.ValidateCredentials(usu_nome, usu_password);
        }


        public static String CamelCase(this String source)
        {
            var arr = source.Split(' ', ',', '_', '-');
            var arrInvalids = new[] { "da", "dos", "de" };
            var retorno = string.Empty;

            foreach (var item in arr)
            {
                if (arrInvalids.Contains(item))
                {
                    retorno += item + " ";
                    continue;
                }
                retorno += item.Substring(0, 1).ToUpper() + item.Substring(1, item.Length - 1).ToLower() + " ";
            }
            return retorno.Trim();
        }
    }



    public class UsersAd
    {
        public string label { get; set; }
        public string value { get; set; }
        public string CanonicalName { get; set; }
        public string sAMAccountName { get; set; }
        public int UserAccountControl { get; set; }
        public string DistinguishedName { get; set; }
        public string Sala { get; set; }
        public string Departamento { get; set; }
        public string Ramal { get; set; }
        public string Telefone { get; set; }
        public string Coordenadoria { get; set; }
    }
}

