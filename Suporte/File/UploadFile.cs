﻿using System;
using System.IO;
using System.Web;
using Ionic.Zip;
using Suporte.Exceptions;

namespace Suporte.File
{
    public sealed class UploadFile 
    {
        private string path { get; set; } 
        private HttpPostedFileBase file { get; set; }

        public UploadFile(string _path, HttpPostedFileBase _file)
        {
            path = _path;
            file = _file; 
        }
        
        /// <summary>
        /// Responsavel por realizar o upload do arquivo no servidor
        /// </summary>
        /// <param name="nameRealServer">Saida do arquivo com seu definitivo</param>
        /// <returns></returns>
        public bool SAVE(out string nameRealServer)
        {
            try
            {
                nameRealServer = DateTime.Now.ToString("ddMMyyyyHHmmss") + file.FileName;
                file.SaveAs(Path.Combine(path, nameRealServer));
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Responsavel por salvar um arquivo
        /// </summary>
        /// <param name="nomeDesejado">Nome desejado do novo arquivo</param>
        /// <param name="nameRealServer">Nome pelo qual foi salvo no servidor</param>
        /// <returns></returns>
        public bool SAVE_MY_NAME(string nomeDesejado, out string nameRealServer)
        {
            try
            {
                nameRealServer = file.FileName;
                file.SaveAs(Path.Combine(path, nomeDesejado));
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            } 
        }

        /// <summary>
        /// Mover arquivo
        /// </summary>
        /// <param name="pathReal">Diretorio do arquivo atual</param>
        /// <param name="pathDestino">Diretorio para onde o arquivo vai</param>
        public void MOVE(string pathReal, string pathDestino)
        {
            try
            {
                System.IO.File.Move(pathReal, pathDestino);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// VALIDA SE O ARQUIVO EXISTE ENTÃO DELETA
        /// </summary>
        /// <param name="strFile"></param>
        public static void FILE_EXISTS(string strFile)
        {
            try
            {
                //Se arquivo não existir
                if (!System.IO.File.Exists(strFile))
                {
                    using (FileStream fs = System.IO.File.Create(strFile))
                    {
                    }
                }
                else
                {
                    System.IO.File.Delete(strFile);
                    using (FileStream fs = System.IO.File.Create(strFile))
                    {

                    }
                }
            }
            catch (CustomException ce)
            {
                throw ce;
            }
        }

        /// <summary>
        /// VALIDA SE O DIRETORIO NÂO EXISTE E CRIA O DIRETORIO
        /// </summary>
        /// <param name="diretorio"></param>
        public static void DIRECTORY_EXISTS(string diretorio)
        {
            try
            {
                //VERIFICA DIRETORIO DOS ARQUIVOS
                var dirInfo = new DirectoryInfo(diretorio); 
                //SE O DIRETORIO NÃO EXISTIR IRA CRIA UM
                if (!dirInfo.Exists)
                    dirInfo.Create();
            }
            catch (CustomException ce)
            {
                throw ce;
            }
        }

        /// <summary>
        /// VERIFICA SE O ARQUIVO JA ESTA SENDO USADO
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns></returns>
        public static bool IsFileOpen(string filePath)
        {
            var fileOpened = false;

            try
            {
                var fs = System.IO.File.OpenWrite(filePath);
                fs.Close();
            }
            catch (IOException ex)
            {
                fileOpened = true;
            }

            return fileOpened;
        }

        public static void ZipFile(FileInfo file)
        {
            try
            {
                using (ZipFile zip = new ZipFile())
                {
                    //zip.UseZip64WhenSaving = Zip64Option.AsNecessary;

                    // Add the file to the Zip archive's root folder.
                    zip.AddFile(file.FullName);
                    //zip.AddFile(file.Name, file.Directory.ToString());
                    // Save the Zip file.
                    zip.Save(file.Directory + @"\" + file.Name.Split('.')[0] + ".zip");

                }
            }
            catch (System.Exception)
            {
                throw new System.Exception("Ocorreu um erro ao compactar o arquivo.");
            }
        }

        public static void ZipFile(FileInfo file, string outDirectory)
        {
            try
            {
                using (ZipFile zip = new ZipFile())
                {
                    zip.UseZip64WhenSaving = Zip64Option.AsNecessary;

                    // Add the file to the Zip archive's root folder.
                    zip.AddFile(file.FullName);
                    // Save the Zip file.
                    zip.Save(outDirectory + @"\" + file.Name.Split('.')[0] + ".zip");
                }
            }
            catch (System.Exception)
            {
                throw new System.Exception("Ocorreu um erro ao compactar o arquivo.");
            }
        }

        public static void UnZipFile(FileInfo file)
        {
            try
            {
                using (var zip = Ionic.Zip.ZipFile.Read(file.FullName))
                {
                    foreach (ZipEntry e in zip)
                    {
                        e.Extract(file.Directory.ToString(), ExtractExistingFileAction.OverwriteSilently);
                        // overwrite == true
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Ocorreu um erro ao descompactar o arquivo.</ br>" + ex.Message);
            }
        }

        public static void UnZipFile(FileInfo file, string outDirectory)
        {
            try
            {
                using (ZipFile zip = Ionic.Zip.ZipFile.Read(file.FullName))
                {
                    foreach (ZipEntry e in zip)
                    {
                        e.Extract(outDirectory, ExtractExistingFileAction.OverwriteSilently); // overwrite == true
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Ocorreu um erro ao descompactar o arquivo.</ br>" + ex.Message); ;
            }
        }
    }
}
