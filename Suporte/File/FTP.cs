﻿using System;
using System.IO;
using System.Net;
using System.Text;
using Suporte.Exceptions;
using Suporte.MsgBox;
using Suporte.MsgBox.AspNet;

namespace Suporte.File
{
    public sealed class FTP : IDisposable
    {
        private string _domain { get; set; }
        private string _user { get; set; }
        private string _password { get; set; }
        private string _file { get; set; }
        private string _msgerror { get; set; }

        public string MessageError { get { return _msgerror; } }

        public FTP(string domain, string user, string password, string file)
        {
            _domain = domain;
            _user = user;
            _password = password;
            _file = file;
        }

        public FTP(string domain, string user, string password)
        {
            _domain = domain;
            _user = user;
            _password = password;
        }
  
        /// <summary>
        /// Metodo responsavel de excluir arquivo do FTP
        /// </summary>
        /// <param name="file">Nome do arquivo</param>
        public void DeleteFile(string file)
        {
            try
            {
                var request = (FtpWebRequest)WebRequest.Create(_domain + (_domain.EndsWith("/") ? "" : "/") + file);
                request.Credentials = new NetworkCredential(_user, _password);
                request.Method = WebRequestMethods.Ftp.DeleteFile;

                FtpWebResponse responseFileDelete;
                if (CheckFileExists(request))
                    responseFileDelete = (FtpWebResponse)request.GetResponse();
            }
            catch (Exception )
            {
                throw new CustomException("Erro ao deletar arquivo no FTP.", MessageBox.MessageType.error);
            }
        }
  
        /// <summary>
        /// Metodo responsavel de enviar arquivo do FTP
        /// </summary>
        /// <param name="fileNamePaste">Diretorio + "/" + Nome do arquivo</param>
        public bool SendFile(string fileNamePaste)
        {
            try
            {
                FileInfo fileInf = new FileInfo(_file);
                FtpWebRequest request = (FtpWebRequest)WebRequest.Create(_domain + (_domain.EndsWith("/") ? "" : "/") + fileNamePaste);
                request.Method = WebRequestMethods.Ftp.UploadFile;
                request.Credentials = new NetworkCredential(_user, _password);
                request.UsePassive = true;
                request.UseBinary = true;
                request.KeepAlive = true;
                request.ContentLength = fileInf.Length;

                DeleteFile(fileNamePaste);

                Stream responseStream = request.GetRequestStream();
                byte[] buffer = new byte[2048];

                FileStream fs = fileInf.OpenRead();
                try
                {
                    int readCount = fs.Read(buffer, 0, buffer.Length);
                    while (readCount > 0)
                    {
                        responseStream.Write(buffer, 0, readCount);
                        readCount = fs.Read(buffer, 0, buffer.Length);
                    }
                }
                finally
                {
                    fs.Close();
                    responseStream.Close();
                }
                return true;
            }
            catch (Exception)
            {
                throw new CustomException("Erro ao enviar arquivo no FTP.", MessageBox.MessageType.error);
            }
        }

        /// <summary>
        /// Metodo responsavel de fazer download do arquivo arquivo do FTP
        /// </summary>
        /// <param name="filePath"></param>
        /// <param name="fileName"></param>
        private void DownloadFile(string filePath, string fileName)
        {
            FtpWebRequest reqFTP;
            try
            {
                //filePath = <<The full path where the file is to be created. the>>,

                //fileName = <<Name of the file to be createdNeed not name on FTP server. name name()>>

                FileStream outputStream = new FileStream(filePath + "\\" + fileName, FileMode.Create);
                reqFTP = (FtpWebRequest)FtpWebRequest.Create(new Uri("ftp://" + _domain + "/" + fileName));
                reqFTP.Method = WebRequestMethods.Ftp.DownloadFile;
                reqFTP.UseBinary = true;
                reqFTP.Credentials = new NetworkCredential(_user, _password);
                FtpWebResponse response = (FtpWebResponse)reqFTP.GetResponse();
                Stream ftpStream = response.GetResponseStream();
                long cl = response.ContentLength;
                int bufferSize = 2048;
                int readCount;
                byte[] buffer = new byte[bufferSize];
                readCount = ftpStream.Read(buffer, 0, bufferSize);
                while (readCount > 0)
                {
                    outputStream.Write(buffer, 0, readCount);
                    readCount = ftpStream.Read(buffer, 0, bufferSize);
                }
                ftpStream.Close();
                outputStream.Close();
                response.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Metodo que retorna todos os arquivos e pastas do diretorio
        /// </summary>
        /// <returns></returns>
        public string[] GetFileList()
        {
            string[] downloadFiles;
            StringBuilder result = new StringBuilder();
            FtpWebRequest reqFTP;
            try
            {
                reqFTP = (FtpWebRequest)FtpWebRequest.Create(new Uri("ftp://" + _domain + "/"));
                reqFTP.UseBinary = true;
                reqFTP.Credentials = new NetworkCredential(_user, _password);
                reqFTP.Method = WebRequestMethods.Ftp.ListDirectory;
                WebResponse response = reqFTP.GetResponse();
                StreamReader reader = new StreamReader(response.GetResponseStream());
                string line = reader.ReadLine();
                while (line != null)
                {
                    result.Append(line);
                    result.Append("\n");
                    line = reader.ReadLine();
                }
                // to remove the trailing '\n'
                result.Remove(result.ToString().LastIndexOf('\n'), 1);
                reader.Close();
                response.Close();
                return result.ToString().Split('\n');
            }

            catch (Exception ex)
            { 
                throw ex; 
            }

        }

        /// <summary>
        /// Metodo que verifica se existe algum arquivo
        /// </summary>
        /// <returns></returns
        private static bool CheckFileExists(WebRequest request)
        {
            try
            {
                request.GetResponse();
                return true;
            }
            catch
            {
                return false;
            }
        }
 
        public void Dispose()
        {            
            GC.SuppressFinalize(this);
        }
    }
}
