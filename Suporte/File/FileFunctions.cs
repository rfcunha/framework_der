﻿using System;
using System.Data;
using System.IO;
using System.Text;
using System.Web;
using Ionic.Zip;
using Suporte.Exceptions;

namespace Suporte.File
{
    public sealed class FileFunctions 
    {
        private string path { get; set; } 
        private HttpPostedFileBase file { get; set; }

        public FileFunctions(string _path, HttpPostedFileBase _file)
        {
            path = _path;
            file = _file; 
        }
        
        /// <summary>
        /// Responsavel por realizar o upload do arquivo no servidor
        /// </summary>
        /// <param name="nameRealServer">Saida do arquivo com seu definitivo</param>
        /// <returns></returns>
        public bool SAVE(out string nameRealServer)
        {
            try
            {
                nameRealServer = DateTime.Now.ToString("ddMMyyyyHHmmss") + file.FileName;
                file.SaveAs(Path.Combine(path, nameRealServer));
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Responsavel por salvar um arquivo
        /// </summary>
        /// <param name="nomeDesejado">Nome desejado do novo arquivo</param>
        /// <param name="nameRealServer">Nome pelo qual foi salvo no servidor</param>
        /// <returns></returns>
        public bool SAVE_MY_NAME(string nomeDesejado, out string nameRealServer)
        {
            try
            {
                nameRealServer = file.FileName;
                file.SaveAs(Path.Combine(path, nomeDesejado));
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            } 
        }

        /// <summary>
        /// Mover arquivo
        /// </summary>
        /// <param name="pathReal">Diretorio do arquivo atual</param>
        /// <param name="pathDestino">Diretorio para onde o arquivo vai</param>
        public void MOVE(string pathReal, string pathDestino)
        {
            try
            {
                System.IO.File.Move(pathReal, pathDestino);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// VALIDA SE O ARQUIVO EXISTE ENTÃO DELETA
        /// </summary>
        /// <param name="strFile"></param>
        public static void FILE_EXISTS(string strFile)
        {
            try
            {
                //Se arquivo não existir
                if (!System.IO.File.Exists(strFile))
                {
                    using (FileStream fs = System.IO.File.Create(strFile))
                    {
                    }
                }
                else
                {
                    System.IO.File.Delete(strFile);
                    using (FileStream fs = System.IO.File.Create(strFile))
                    {

                    }
                }
            }
            catch (CustomException ce)
            {
                throw ce;
            }
        }

        /// <summary>
        /// VALIDA SE O DIRETORIO NÂO EXISTE E CRIA O DIRETORIO
        /// </summary>
        /// <param name="diretorio"></param>
        public static void DIRECTORY_EXISTS(string diretorio)
        {
            try
            {
                //VERIFICA DIRETORIO DOS ARQUIVOS
                var dirInfo = new DirectoryInfo(diretorio); 
                //SE O DIRETORIO NÃO EXISTIR IRA CRIA UM
                if (!dirInfo.Exists)
                    dirInfo.Create();
            }
            catch (CustomException ce)
            {
                throw ce;
            }
        }

        /// <summary>
        /// VERIFICA SE O ARQUIVO JA ESTA SENDO USADO
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns></returns>
        public static bool IS_FILE_OPEN(string filePath)
        {
            var fileOpened = false;

            try
            {
                var fs = System.IO.File.OpenWrite(filePath);
                fs.Close();
            }
            catch (IOException ex)
            {
                fileOpened = true;
            }

            return fileOpened;
        }

        /// <summary>
        /// COMPACTA UM ARQUIVO
        /// </summary>
        /// <param name="file"></param>
        public static void ZIP_FILE(FileInfo file)
        {
            try
            {
                using (ZipFile zip = new ZipFile())
                {
                    //zip.UseZip64WhenSaving = Zip64Option.AsNecessary;

                    // Add the file to the Zip archive's root folder.
                    zip.AddFile(file.DirectoryName + @"\" + file.Name, string.Empty);
                    //zip.AddFile(file.Name, file.Directory.ToString());
                    // Save the Zip file.
                    zip.Save(file.DirectoryName + @"\" + file.Name.Split('.')[0] + ".zip");

                }
            }
            catch (System.Exception)
            {
                throw new System.Exception("Ocorreu um erro ao compactar o arquivo.");
            }
        }

        /// <summary>
        /// COMPACTA UM ARQUIVO E SALVA NO DIRETORIO
        /// </summary>
        /// <param name="file"></param>
        /// <param name="OutDirectory"></param>
        public static void ZIP_FILE(FileInfo file, string OutDirectory)
        {
            try
            {
                using (ZipFile zip = new ZipFile())
                {
                    zip.UseZip64WhenSaving = Zip64Option.AsNecessary;

                    // Add the file to the Zip archive's root folder.
                    zip.AddFile(file.FullName);
                    // Save the Zip file.
                    zip.Save(OutDirectory + @"\" + file.Name.Split('.')[0] + ".zip");
                }
            }
            catch (System.Exception)
            {
                throw new System.Exception("Ocorreu um erro ao compactar o arquivo.");
            }
        }

        /// <summary>
        /// DESCOMPACTA O ARQUIVO
        /// </summary>
        /// <param name="file"></param>
        public static void UN_ZIP_FILE(FileInfo file)
        {
            try
            {
                using (Ionic.Zip.ZipFile zip = Ionic.Zip.ZipFile.Read(file.FullName))
                {
                    foreach (ZipEntry e in zip)
                    {
                        e.Extract(file.Directory.ToString(), ExtractExistingFileAction.OverwriteSilently);
                        // overwrite == true
                    }
                }
            }
            catch (System.Exception)
            {
                throw new System.Exception("Ocorreu um erro ao descompactar o arquivo.");
            }
        }

        /// <summary>
        /// DESCOMPACTA O ARQUIVO E SALVA NO DIRETORIO
        /// </summary>
        /// <param name="file"></param>
        /// <param name="OutDirectory"></param>
        public static void UN_ZIP_FILE(FileInfo file, string OutDirectory)
        {
            try
            {
                using (Ionic.Zip.ZipFile zip = Ionic.Zip.ZipFile.Read(file.FullName))
                {
                    foreach (ZipEntry e in zip)
                    {
                        e.Extract(OutDirectory, ExtractExistingFileAction.OverwriteSilently); // overwrite == true
                    }
                }
            }
            catch (System.Exception)
            {
                throw new System.Exception("Ocorreu um erro ao descompactar o arquivo.");
            }
        }

        /// <summary>
        /// CONVERTE UM DATATABLE EM EXCEL
        /// </summary>
        /// <param name="dtExcel"></param>
        /// <param name="fileName"></param>
        public static void EXPORT_TO_EXCEL(DataTable dtExcel, string fileName)
        {
            try
            {
                string attachment = "attachment; filename=" + fileName + ".xls";

                HttpContext.Current.Response.ClearContent();
                HttpContext.Current.Response.AddHeader("content-disposition", attachment);
                HttpContext.Current.Response.ContentType = "application/vnd.ms-excel";
                HttpContext.Current.Response.ContentEncoding = Encoding.GetEncoding("ISO-8859-1");
                HttpContext.Current.Response.Charset = "ISO-8859-1";


                string tab = "";
                foreach (DataColumn dc in dtExcel.Columns)
                {
                    HttpContext.Current.Response.Write(tab + dc.ColumnName);
                    tab = "\t";
                }

                HttpContext.Current.Response.Write("\n");

                int i;
                foreach (DataRow dr in dtExcel.Rows)
                {
                    tab = "";

                    for (i = 0; i < dtExcel.Columns.Count; i++)
                    {
                        HttpContext.Current.Response.Write(tab + dr[i]);
                        tab = "\t";
                    }

                    HttpContext.Current.Response.Write("\n");
                }

                HttpContext.Current.Response.Flush();
                HttpContext.Current.Response.Close();
            }
            catch (Exception)
            {
                throw new Exception("Ocorreu um erro ao criar o arquivo excel.");
            }
        }

        /// <summary>
        /// CONVERTE UM DATATABLE EM CSV
        /// </summary>
        /// <param name="dtExcel"></param>
        /// <param name="fileName"></param>
        public static void EXPORT_TO_CSV(DataTable dtExcel, string fileName)
        {
            try
            {
                StringBuilder sb = new StringBuilder();
                foreach (DataColumn col in dtExcel.Columns)
                {
                    sb.Append(col.ColumnName + ',');
                }

                sb.Remove(sb.Length - 1, 1);
                sb.AppendLine();

                foreach (DataRow row in dtExcel.Rows)
                {
                    for (int i = 0; i < dtExcel.Columns.Count; i++)
                    {
                        sb.Append(row[i] + ",");
                    }

                    sb.AppendLine();
                }

                string attachment = "attachment; filename=" + fileName + ".csv";

                HttpContext.Current.Response.Clear();
                HttpContext.Current.Response.Buffer = true;

                HttpContext.Current.Response.AddHeader("content-disposition", attachment);
                HttpContext.Current.Response.Charset = "";
                HttpContext.Current.Response.ContentType = "application/text";
                HttpContext.Current.Response.ContentEncoding = Encoding.GetEncoding("ISO-8859-1");
                HttpContext.Current.Response.Charset = "ISO-8859-1";
                HttpContext.Current.Response.Output.Write(sb.ToString());
                HttpContext.Current.Response.Flush();
                HttpContext.Current.Response.End();
            }
            catch (Exception)
            {
                throw new Exception("Ocorreu um erro ao criar o aquivo csv.");
            }
        }
    }
}
