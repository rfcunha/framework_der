﻿using System.Transactions;

namespace Suporte.Transaction
{
    public static class TransactionScopeUtils
    {
        /// <summary>
        /// Metodo responsavel por criar uma TRANSCTION SCOPE
        /// </summary>
        /// <returns></returns>
        public static TransactionScope CreateTransactionScope()
        {
            var transactionOptions = new TransactionOptions
            {
                IsolationLevel = IsolationLevel.ReadCommitted,
                Timeout = TransactionManager.MaximumTimeout
            };

            return new TransactionScope(TransactionScopeOption.Required, transactionOptions);
        }
    }
}
