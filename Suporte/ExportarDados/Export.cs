﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.IO;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Suporte.ExportarDados
{
    /// <summary>
    /// Enum com as extensões dos arquivos
    /// </summary>
    public enum TypeFormat
    {
        [Description(".xlsx")]
        xlsx,
        [Description(".xls")]
        xls,
        [Description(".xlsm")]
        xlsm,
        [Description(".cvs")]
        cvs
    }

    public class Export
    {
        /// <summary>
        /// Metodo para transformar uma LISTA em um "EXCEL" 
        /// </summary>
        /// <typeparam name="T">Tipo</typeparam>
        /// <param name="nomeArquivo">Nome do arquivo ao fazer download</param>
        /// <param name="dados">Lista a ser passada</param>
        /// <param name="extensao">Extensão do arquivo</param>
        public void ExportTo<T>(string nomeArquivo, List<T> dados, TypeFormat extensao = TypeFormat.xls)
        {
            try
            {
                var ext = ToDescriptionString(extensao);
                //Limpar o conteudo
                HttpContext.Current.Response.Clear();
                //Seta o ContentType para xls
                HttpContext.Current.Response.ContentType = "application/vnd.ms-excel";
                //Seta o tipo e o nome do arquivo
                HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment;filename=" + nomeArquivo + ext);
                //Abaixo codifica os caracteres para o alfabeto latino
                HttpContext.Current.Response.ContentEncoding = Encoding.GetEncoding("ISO-8859-1");
                HttpContext.Current.Response.Charset = "ISO-8859-1";

                // Cria o StringWriter
                using (var sw = new StringWriter())
                {
                    using (var htw = new HtmlTextWriter(sw))
                    {
                        //Cria um GridView
                        var gv = new GridView();
                        gv.DataSource = dados;
                        gv.DataBind();
                        //Renderiza
                        gv.RenderControl(htw);
                        HttpContext.Current.Response.Write(sw.ToString());
                        HttpContext.Current.Response.End();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Metodo para transformar uma DATATABLE em um "EXCEL" 
        /// </summary>
        /// <typeparam name="T">Tipo</typeparam>
        /// <param name="nomeArquivo">Nome do arquivo ao fazer download</param>
        /// <param name="dados">DataTable a ser passada</param>
        /// <param name="extensao">Extensão do arquivo</param>
        public void ExportTo(string nomeArquivo, DataTable dados, TypeFormat extensao = TypeFormat.xls)
        {
            try
            {
                var ext = ToDescriptionString(extensao);
                //Limpar o conteudo
                HttpContext.Current.Response.Clear();
                //Seta o ContentType para xls
                HttpContext.Current.Response.ContentType = "application/vnd.ms-excel";
                //Seta o tipo e o nome do arquivo
                HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment;filename=" + nomeArquivo + ext);
                //Abaixo codifica os caracteres para o alfabeto latino
                HttpContext.Current.Response.ContentEncoding = Encoding.GetEncoding("ISO-8859-1");
                HttpContext.Current.Response.Charset = "ISO-8859-1";

                // Cria o StringWriter
                using (var sw = new StringWriter())
                {
                    using (var htw = new HtmlTextWriter(sw))
                    {
                        //Cria um GridView
                        var gv = new GridView();
                        gv.DataSource = dados;
                        gv.DataBind();
                        //Renderiza
                        gv.RenderControl(htw);
                        HttpContext.Current.Response.Write(sw.ToString());
                        HttpContext.Current.Response.End();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// METODO PARA CONVERTER LISTA PARA TXT E SALVAL EM UM LOCAL INDICADO
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="lista"></param>
        /// <param name="local"></param>
        /// <param name="nomeArquivo"></param>
        public static bool SaveListTxt<T>(T lista, string local, string nomeArquivo)
        {
            try
            {
                File.FileFunctions.DIRECTORY_EXISTS(local);

                if (!System.IO.File.Exists(local + nomeArquivo))
                    System.IO.File.Delete(local + nomeArquivo);

                using (StreamWriter writer = System.IO.File.CreateText(local + nomeArquivo))
                {
                    var gv = new GridView();
                    gv.DataSource = lista;
                    gv.DataBind();
                    //gv.RenderControl(hw);

                    var sLine = string.Empty;

                    for (int r = 0; r <= gv.Rows.Count - 1; r++)
                    {
                        for (int c = 0; c <= gv.Rows[r].Cells.Count - 1; c++)
                        {
                            sLine += gv.Rows[r].Cells[c].Text;

                            if (c != gv.Columns.Count - 1)
                            {
                                sLine = sLine;
                            }
                        }

                        writer.WriteLine(sLine);
                        sLine = "";
                    }

                    writer.Close();

                    return true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private string ToDescriptionString(TypeFormat val)
        {
            DescriptionAttribute[] attributes = (DescriptionAttribute[])val.GetType().GetField(val.ToString()).GetCustomAttributes(typeof(DescriptionAttribute), false);
            return attributes.Length > 0 ? attributes[0].Description : string.Empty;
        }
    } 
}
