﻿using System;
using Suporte.MsgBox;

namespace Suporte.Exceptions
{
    public class CustomException : Exception
    {
        private string _titulo;

        public string GetTitulo
        {
            get { return _titulo; }
        }

        //private readonly MessageBox.MessageType _tipoErroMsgBox;
        private readonly bootStrapMessageType _tipoErroBootStrapDialog;

        public bootStrapMessageType GetTipoErroBootStrapDialog
        {
            get { return _tipoErroBootStrapDialog; }
        }

        public CustomException() : base()
        {
        }

        /// <summary>
        /// EXECUTA EXCEPTION COM MENSAGEM
        /// </summary>
        /// <param name="message"></param>
        public CustomException(string message) : base(message)
        {
            //_tipoErroMsgBox = MessageBox.MessageType.info;
            _tipoErroBootStrapDialog = bootStrapMessageType.TYPE_INFO;

            _titulo = "Informativo";
        }


        public CustomException(string message, bootStrapMessageType tipoErroBootStrapDialog) : base(message)
        {
            _tipoErroBootStrapDialog = tipoErroBootStrapDialog;

            switch (tipoErroBootStrapDialog)
            {
                case bootStrapMessageType.TYPE_INFO:
                    _titulo = "Informativo";
                    break;

                case bootStrapMessageType.TYPE_DANGER:
                    _titulo = "Erro";
                    break;

                case bootStrapMessageType.TYPE_WARNING:
                    _titulo = "Atenção";
                    break;
            }
        }
    }
}
