﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace Suporte.Criptografia
{
    public class Cripto
    {  
        /// <summary>
        /// Função responsavel por fazer a Criptografia
        /// </summary>
        /// <param name="texto">Texto</param>
        /// <param name="chave">Chave</param>
        /// <returns>Dado criptografado</returns>
        public static string Encrypt(string texto, string chave)
        {
            try
            {
                byte[] keyArray;
                byte[] toEncryptArray = Encoding.UTF8.GetBytes(texto);


                //System.Windows.Forms.MessageBox.Show(key);
                //If hashing use get hashcode regards to your key
                var hashmd5 = new MD5CryptoServiceProvider();
                keyArray = hashmd5.ComputeHash(Encoding.UTF8.GetBytes(chave));

                //Always release the resources and flush data
                // of the Cryptographic service provide. Best Practice
                hashmd5.Clear();

                var tdes = new TripleDESCryptoServiceProvider();

                //set the secret key for the tripleDES algorithm
                tdes.Key = keyArray;

                //mode of operation. there are other 4 modes.
                //We choose ECB(Electronic code Book)
                tdes.Mode = CipherMode.ECB;

                //padding mode(if any extra byte added)
                tdes.Padding = PaddingMode.PKCS7;

                var cTransform = tdes.CreateEncryptor();

                //transform the specified region of bytes array to resultArray
                byte[] resultArray = cTransform.TransformFinalBlock(toEncryptArray, 0, toEncryptArray.Length);

                //Release resources held by TripleDes Encryptor
                tdes.Clear();
                var value =  Convert.ToBase64String(resultArray, 0, resultArray.Length);
                //Return the encrypted data into unreadable string format
                return value;
            }
            catch (Exception)
            {
                throw new Exception("Ocorreu um erro ao encriptografar. Entre em contato com o suporte.");    
            }
        }

        /// <summary>
        /// Função responsavel por Descriptografar
        /// </summary>
        /// <param name="texto">Texto</param>
        /// <param name="chave">Chave</param>
        /// <returns>Dado Descriptografados</returns>
        public static string Decrypt(string texto, string chave)
        {
            try
            {
                byte[] keyArray;

                //get the byte code of the string
                byte[] toEncryptArray = Convert.FromBase64String(texto);

                //if hashing was used get the hash code with regards to your key
                var hashmd5 = new MD5CryptoServiceProvider();
                keyArray = hashmd5.ComputeHash(Encoding.UTF8.GetBytes(chave));

                //release any resource held by the MD5CryptoServiceProvider
                hashmd5.Clear();

                var tdes = new TripleDESCryptoServiceProvider();

                //set the secret key for the tripleDES algorithm
                tdes.Key = keyArray;

                //mode of operation. there are other 4 modes. 
                //We choose ECB(Electronic code Book)
                tdes.Mode = CipherMode.ECB;

                //padding mode(if any extra byte added)
                tdes.Padding = PaddingMode.PKCS7;

                ICryptoTransform cTransform = tdes.CreateDecryptor();
                byte[] resultArray = cTransform.TransformFinalBlock(toEncryptArray, 0, toEncryptArray.Length);

                //Release resources held by TripleDes Encryptor                
                tdes.Clear();

                var value = Encoding.UTF8.GetString(resultArray);
                //Return the encrypted data into unreadable string format
                return value;
                 
            }
            catch (Exception)
            {
                throw new Exception("Ocorreu um erro ao descriptografar. Entre em contato com o suporte.");  
                
            } 
        }



        /// <summary>
        /// Função responsavel por fazer a Criptografia LOGIN DE SISTEMAS
        /// </summary>
        /// <param name="texto">Texto</param> 
        /// <returns>Dado criptografado</returns>
        public static string Encrypt(string texto)
        {
            try
            {
                byte[] keyArray;
                byte[] toEncryptArray = Encoding.UTF8.GetBytes(texto);


                //System.Windows.Forms.MessageBox.Show(key);
                //If hashing use get hashcode regards to your key
                var hashmd5 = new MD5CryptoServiceProvider();
                keyArray = hashmd5.ComputeHash(Encoding.UTF8.GetBytes("Doce"));

                //Always release the resources and flush data
                // of the Cryptographic service provide. Best Practice
                hashmd5.Clear();

                var tdes = new TripleDESCryptoServiceProvider();

                //set the secret key for the tripleDES algorithm
                tdes.Key = keyArray;

                //mode of operation. there are other 4 modes.
                //We choose ECB(Electronic code Book)
                tdes.Mode = CipherMode.ECB;

                //padding mode(if any extra byte added)
                tdes.Padding = PaddingMode.PKCS7;

                var cTransform = tdes.CreateEncryptor();

                //transform the specified region of bytes array to resultArray
                byte[] resultArray = cTransform.TransformFinalBlock(toEncryptArray, 0, toEncryptArray.Length);

                //Release resources held by TripleDes Encryptor
                tdes.Clear();
                var value = Convert.ToBase64String(resultArray, 0, resultArray.Length);
                //Return the encrypted data into unreadable string format
                return value;
            }
            catch (Exception)
            {
                throw new Exception("Ocorreu um erro ao encriptografar. Entre em contato com o suporte.");
            }
        }

        /// <summary>
        /// Função responsavel por Descriptografar LOGIN DE SISTEMAS
        /// </summary>
        /// <param name="texto">Texto</param> 
        /// <returns>Dado Descriptografados</returns>
        public static string Decrypt(string texto)
        {
            try
            {
                byte[] keyArray;

                //get the byte code of the string
                byte[] toEncryptArray = Convert.FromBase64String(texto);

                //if hashing was used get the hash code with regards to your key
                var hashmd5 = new MD5CryptoServiceProvider();
                keyArray = hashmd5.ComputeHash(Encoding.UTF8.GetBytes("Doce"));

                //release any resource held by the MD5CryptoServiceProvider
                hashmd5.Clear();

                var tdes = new TripleDESCryptoServiceProvider();

                //set the secret key for the tripleDES algorithm
                tdes.Key = keyArray;

                //mode of operation. there are other 4 modes. 
                //We choose ECB(Electronic code Book)
                tdes.Mode = CipherMode.ECB;

                //padding mode(if any extra byte added)
                tdes.Padding = PaddingMode.PKCS7;

                ICryptoTransform cTransform = tdes.CreateDecryptor();
                byte[] resultArray = cTransform.TransformFinalBlock(toEncryptArray, 0, toEncryptArray.Length);

                //Release resources held by TripleDes Encryptor                
                tdes.Clear();

                var value = Encoding.UTF8.GetString(resultArray);
                //Return the encrypted data into unreadable string format
                return value;

            }
            catch (Exception)
            {
                throw new Exception("Ocorreu um erro ao descriptografar. Entre em contato com o suporte.");

            }
        }
    }
}
