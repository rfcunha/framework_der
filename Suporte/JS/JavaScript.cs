﻿using System;
using System.Collections;
using System.Web;
using System.Web.UI;

namespace Suporte.JS
{
    public class JavaScript
    {

        private static Hashtable handlerPages = new Hashtable();

        private class ScriptItem
        {
            public string Script;

            public ScriptItem(string script)
            {
                Script = script;
            }
        }

        public static void ExecutaScript(string script)
        {
            if (!(handlerPages.Contains(HttpContext.Current.Handler)))
            {
                var currentPage = (Page)HttpContext.Current.Handler;
                if (currentPage != null)
                {
                    var queue = new Queue();
                    queue.Enqueue(new ScriptItem(script));
                    handlerPages.Add(HttpContext.Current.Handler, queue);
                    currentPage.PreRenderComplete += CurrentPagePreRender;
                }
            }
            else
            {
                var queue = ((Queue)(handlerPages[HttpContext.Current.Handler]));
                queue.Enqueue(new ScriptItem(script));
            }
        }

        private static void CurrentPagePreRender(object sender, EventArgs e)
        {
            var currentPage = (Page)HttpContext.Current.Handler;

            var queue = ((Queue)(handlerPages[HttpContext.Current.Handler]));

            if (queue != null)
            {
                String script = "";

                while (queue.Count > 0)
                {
                    var iMsg = (ScriptItem)queue.Dequeue();
                    script += iMsg.Script;
                }

                ScriptManager.RegisterStartupScript(currentPage, currentPage.GetType(), Guid.NewGuid().ToString(), script, true);
            }
        }
    }
}
