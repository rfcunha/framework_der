﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Web;
using Suporte.Exceptions;
using Suporte.MsgBox;
using Suporte.MsgBox.AspNet;

namespace Suporte.Formatacao
{
    public static class FormartaDados
    {
        #region FORMAT DADOS

        public static string FormatarCep(this string cep, bool removeFormatacao)
        {
            try
            {
                if (removeFormatacao)
                    cep = Regex.Replace(cep, @"[^0-9,]+", string.Empty);
                else
                    cep = cep.Insert(5, "-");
                
                return cep;
            }
            catch
            {
                return null;
            }
        }

        public static string FormatarTelefone(this string telefone, bool removeFormatacao)
        {
            try
            {
                if (removeFormatacao)
                    telefone = Regex.Replace(telefone, @"[^0-9,]+", string.Empty);
                else
                    telefone = telefone.Insert(0, "(").Insert(3, ")").Insert(8, "-");

                return telefone;
            }
            catch
            {
                return null;
            }
        }

        public static string FormatarCelular(this string celular, bool removeFormatacao)
        {
            try
            {
                if (removeFormatacao)
                    celular = Regex.Replace(celular, @"[^0-9,]+", string.Empty);
                else
                    celular = celular.Insert(0, "(").Insert(3, ")").Insert(9, "-");

                return celular;
            }
            catch
            {
                return null;
            }
        }

        public static string FormatarCpf(this string cpf, bool removeFormatacao)
        {
            try
            {
                //cpf = cpf.Replace(".", "").Replace("/", "").Replace("-", "");
                if (removeFormatacao)
                    cpf = Regex.Replace(cpf, @"[^0-9,]+", string.Empty);
                else
                {
                    cpf = cpf.Insert(3, ".").Insert(7, ".").Insert(11, "-");
                }

                return cpf;
            }
            catch
            {
                return null;
            }
        }

        public static bool ValidaCpf(string cpf)
        {
            int[] CalcARR = null;
            int Sum = 0;
            int DV1 = 0;
            int DV2 = 0;
            if (string.IsNullOrEmpty(cpf))
            {
                return true;
            }
            if (long.Parse(cpf) == 0)
            {
                return false;
            }
            if (cpf.Length != 11)
            {
                cpf = string.Format("{0:D11}", long.Parse(cpf));
            }
            CalcARR = new int[11];
            for (int x = 0; x < CalcARR.Length; x++)
            {
                CalcARR[x] = int.Parse(cpf[x].ToString());
            }
            Sum = 0;
            for (int x = 1; x <= 9; x++)
            {
                Sum += CalcARR[x - 1] * (11 - x);
            }
            Math.DivRem(Sum, 11, out DV1);
            DV1 = 11 - DV1;
            DV1 = DV1 > 9 ? 0 : DV1;
            if (DV1 != CalcARR[9])
            {
                return false;
            }
            Sum = 0;
            for (int x = 1; x <= 10; x++)
            {
                Sum += CalcARR[x - 1] * (12 - x);
            }
            Math.DivRem(Sum, 11, out DV2);
            DV2 = 11 - DV2;
            DV2 = DV2 > 9 ? 0 : DV2;
            if (DV2 != CalcARR[10])
            {
                return false;
            }
            return true;
        }

        public static string FormatarRg(this string cpf, bool removeFormatacao)
        {
            try
            {
                //cpf = cpf.Replace(".", "").Replace("/", "").Replace("-", "");
                if (removeFormatacao)
                    cpf = Regex.Replace(cpf, @"[^0-9,]+", string.Empty);
                else
                {
                    if (cpf.Length > 8)
                        cpf = cpf.Insert(2, ".").Insert(5, ".").Insert(8, "-");
                    else
                        cpf = cpf.Insert(2, ".").Insert(5, ".").Insert(8, "-");
                }

                return cpf;
            }
            catch
            {
                return null;
            }
        }

        public static string FormatarCnpj(this string cnpj, bool removeFormatacao)
        {
            try
            {
                if (removeFormatacao)
                    cnpj = Regex.Replace(cnpj, @"[^0-9,]+", string.Empty);
                else
                    cnpj = cnpj.Insert(2, ".").Insert(6, ".").Insert(10, "/").Insert(15, "-");
                
                return cnpj;
            }
            catch
            {
                return null;
            }
        }
         
        public static bool ValidaCnpj(string cnpj)
        {
            int[] CalcARR = null;
            int Sum = 0;
            int Multp = 0;
            int DV1 = 0;
            int DV2 = 0;
            if (string.IsNullOrEmpty(cnpj))
            {
                return true;
            }
            if (long.Parse(cnpj) == 0)
            {
                return false;
            }
            if (cnpj.Length != 14)
            {
                cnpj = string.Format("{0:D14}", long.Parse(cnpj));
            }
            CalcARR = new int[14];
            for (int x = 0; x < CalcARR.Length; x++)
            {
                CalcARR[x] = int.Parse(cnpj[x].ToString());
            }
            Multp = 5;
            Sum = 0;
            for (int x = 0; x < 12; x++)
            {
                Sum += CalcARR[x] * Multp;
                Multp--;
                if (Multp < 2)
                {
                    Multp = 9;
                }
            }
            Math.DivRem(Sum, 11, out DV1);
            if (DV1 < 2)
            {
                DV1 = 0;
            }
            else
            {
                DV1 = 11 - DV1;
            }
            if (DV1 != CalcARR[12])
            {
                return false;
            }
            Multp = 6;
            Sum = 0;
            for (int x = 0; x < 13; x++)
            {
                Sum += CalcARR[x] * Multp;
                Multp--;
                if (Multp < 2)
                {
                    Multp = 9;
                }
            }
            Math.DivRem(Sum, 11, out DV2);
            if (DV2 < 2)
            {
                DV2 = 0;
            }
            else
            {
                DV2 = 11 - DV2;
            }
            if (DV2 != CalcARR[13])
            {
                return false;
            }
            return true;
        }

        public static bool ValidaPis(string pis)
        {
            int[] multiplicador = new int[10] { 3, 2, 9, 8, 7, 6, 5, 4, 3, 2 };
            int soma;
            int resto;

            if (pis.Trim().Length != 11)
                return false;

            pis = pis.Trim();
            pis = pis.Replace("-", "").Replace(".", "").PadLeft(11, '0');


            soma = 0;
            for (int i = 0; i < 10; i++)
                soma += int.Parse(pis[i].ToString()) * multiplicador[i];

            resto = soma % 11;

            if (resto < 2)
                resto = 0;
            else
                resto = 11 - resto;

            return pis.EndsWith(resto.ToString());
        }

        public static bool ValidaEmail(string Valor)
        {
            if (string.IsNullOrEmpty(Valor))
            {
                return true;
            }

            bool Valido = false;
            Regex regEx =
                new Regex(@"^[a-zA-Z][\w\.-]*[a-zA-Z0-9]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$",
                    RegexOptions.IgnoreCase);
            Valido = regEx.IsMatch(Valor);
            return Valido;
        }

        public static String Encode(this String source)
        {
            return HttpContext.Current.Server.HtmlEncode(source);
        }

        public static object Encode(this object source)
        {
            return HttpContext.Current.Server.HtmlEncode(source.ToString());
        }

        public static String RemoveCaracteresSpeciais(this String nomeArquivo)
        {
            String normalizedString = nomeArquivo;

            // Prepara a tabela de símbolos.
            var symbolTable = new Dictionary<char, char[]>();
            symbolTable.Add('a', new[] { 'à', 'á', 'ä', 'â', 'ã' });
            symbolTable.Add('c', new[] { 'ç' });
            symbolTable.Add('e', new[] { 'è', 'é', 'ë', 'ê' });
            symbolTable.Add('i', new[] { 'ì', 'í', 'ï', 'î' });
            symbolTable.Add('o', new[] { 'ò', 'ó', 'ö', 'ô', 'õ' });
            symbolTable.Add('u', new[] { 'ù', 'ú', 'ü', 'û' });

            // Substitui os símbolos.
            foreach (char key in symbolTable.Keys)
            {
                foreach (char symbol in symbolTable[key])
                {
                    normalizedString = normalizedString.Replace(symbol, key);
                }
            }

            // Remove os outros caracteres especiais.
            normalizedString = Regex.Replace(normalizedString, "[^0-9a-zA-Z._]+?", "");

            return normalizedString;
        }

        public static String RemoveCaracteresSpeciaisFileUpload(this String nomeArquivo)
        {
            String normalizedString = nomeArquivo.TrimStart().TrimEnd();

            // Prepara a tabela de símbolos.
            var symbolTable = new Dictionary<char, char[]>();
            symbolTable.Add('a', new char[] { 'à', 'á', 'ä', 'â', 'ã' });
            symbolTable.Add('A', new char[] { 'À', 'Á', 'Ä', 'Â', 'Ã' });
            symbolTable.Add('c', new char[] { 'ç' });
            symbolTable.Add('C', new char[] { 'Ç' });
            symbolTable.Add('e', new char[] { 'è', 'é', 'ë', 'ê' });
            symbolTable.Add('E', new char[] { 'È', 'É', 'Ë', 'Ê' });
            symbolTable.Add('i', new char[] { 'ì', 'í', 'ï', 'î' });
            symbolTable.Add('I', new char[] { 'Ì', 'Í', 'Ï', 'Î' });
            symbolTable.Add('o', new char[] { 'ò', 'ó', 'ö', 'ô', 'õ' });
            symbolTable.Add('O', new char[] { 'Ò', 'Ó', 'Ö', 'Ô', 'Õ' });
            symbolTable.Add('u', new char[] { 'ù', 'ú', 'ü', 'û' });
            symbolTable.Add('U', new char[] { 'Ù', 'Ú', 'Ü', 'Û' });

            // Substitui os símbolos.
            foreach (char key in symbolTable.Keys)
            {
                foreach (char symbol in symbolTable[key])
                {
                    normalizedString = normalizedString.Replace(symbol, key);
                }
            }

            // Remove os outros caracteres especiais.
            normalizedString = Regex.Replace(normalizedString, "[^0-9a-zA-Z._]+?", "");

            return normalizedString;
        }

        #endregion

        #region FORMAT OBJETOS

        public static T GetType<T>(object objeto)
        {
            try
            {
                Type tipo = objeto.GetType();

                if (objeto.GetType().Name.ToUpper() == "DBNULL")
                {
                    return default(T);
                }

                return (T)Convert.ChangeType(objeto, tipo);
            }
            catch (Exception)
            {
                throw new CustomException("Ocorreu um erro ao converter o dado.", bootStrapMessageType.TYPE_DANGER);
            }
        }

        public static object SetType<T>(object value)
        {
            if (value == null && typeof(T).IsGenericType && typeof(T).GetGenericTypeDefinition() == typeof(Nullable<>))
                return DBNull.Value;


            if (value == null)
                return DBNull.Value;

            //if (ReferenceEquals(value, string.Empty))
            //    return DBNull.Value;

            //return (T)Convert.ChangeType(value, value.GetType());


            switch (value.GetType().Name)
            {
                case "Boolean":

                    if (Convert.ToBoolean(value) == null)
                        return DBNull.Value;

                    break;

                case "Decimal":

                    if (Convert.ToDecimal(value) == -1)
                        return DBNull.Value;

                    break;

                case "Double":

                    if (Convert.ToDouble(value) == -1)
                        return DBNull.Value;

                    break;

                case "String":

                    if (ReferenceEquals(value, string.Empty))
                        return DBNull.Value;

                    break;

                //case "long":

                //    if (Convert.ToUInt64(value) == -1)
                //        return DBNull.Value;

                //    break;

                case "Int64":

                    if (((Convert.ToInt64(value) == -1) || (Convert.ToInt64(value) == 0)))
                        return DBNull.Value;

                    break;

                case "Int32":

                    if (((Convert.ToInt32(value) == -1) || (Convert.ToInt64(value) == 0)))
                        return DBNull.Value;

                    break;

                case "Int16":

                    if (((Convert.ToInt16(value) == -1) || (Convert.ToInt64(value) == 0)))
                        return DBNull.Value;

                    break;

                case "DateTime":

                    if (Convert.ToDateTime(value) == DateTime.MinValue || Convert.ToDateTime(value) == DateTime.MaxValue)
                        return DBNull.Value;

                    break;

                //case "TimeSpan":

                //    if (Convert.ToDateTime(value) == null)
                //        return DBNull.Value;

                //    break;
            }


            return (T)Convert.ChangeType(value, value.GetType());
        }

        public static int Int(object objeto)
        {
            try
            {
                if (Convert.IsDBNull(objeto))
                    return 0;

                return Convert.ToInt32(objeto);
            }
            catch (Exception)
            {
                throw new CustomException("Ocorreu um erro ao converter a variavel para Int.", bootStrapMessageType.TYPE_DANGER);
            }
        }

        public static int? IntNull(object objeto)
        {
            try
            {
                if (Convert.IsDBNull(objeto))
                    return null;

                return Convert.ToInt32(objeto);
            }
            catch (Exception)
            {
                throw new CustomException("Ocorreu um erro ao converter a variavel para Int.", bootStrapMessageType.TYPE_DANGER);
            }
        }

        public static DateTime Data(object objeto)
        {
            try
            {
                if (Convert.IsDBNull(objeto))
                    return DateTime.MinValue;

                IFormatProvider formato = new CultureInfo("pt-BR", true);
                return Convert.ToDateTime(objeto, formato);
            }
            catch (Exception)
            {
                throw new CustomException("Ocorreu um erro ao converter a variavel para Data.", bootStrapMessageType.TYPE_DANGER);
            }
        }

        public static DateTime? DataNull(object objeto)
        {
            try
            {
                if (Convert.IsDBNull(objeto))
                    return null;

                IFormatProvider formato = new CultureInfo("pt-BR", true);
                return Convert.ToDateTime(objeto, formato);
            }
            catch (Exception)
            {
                throw new CustomException("Ocorreu um erro ao converter a variavel para Data.", bootStrapMessageType.TYPE_DANGER);
            }
        }

        public static decimal Decimal(object objeto)
        {
            try
            {
                if (Convert.IsDBNull(objeto))
                    return 0;

                return Convert.ToDecimal(objeto);
            }
            catch (Exception)
            {
                throw new CustomException("Ocorreu um erro ao converter a variavel para Decimal.", bootStrapMessageType.TYPE_DANGER);
            }
        }

        public static decimal? DecimalNull(object objeto)
        {
            try
            {
                if (Convert.IsDBNull(objeto))
                    return null;

                return Convert.ToDecimal(objeto);
            }
            catch (Exception)
            {
                throw new CustomException("Ocorreu um erro ao converter a variavel para Decimal.", bootStrapMessageType.TYPE_DANGER);
            }
        }

        public static bool Booleano(object objeto)
        {
            try
            {
                if (Convert.IsDBNull(objeto))
                    return false;

                return Convert.ToBoolean(objeto);
            }
            catch (Exception)
            {
                throw new CustomException("Ocorreu um erro ao converter a variavel para Boleano.", bootStrapMessageType.TYPE_DANGER);
            }
        }

        public static bool? BooleanoNull(object objeto)
        {
            try
            {
                if (Convert.IsDBNull(objeto))
                    return null;

                return Convert.ToBoolean(objeto);
            }
            catch (Exception)
            {
                throw new CustomException("Ocorreu um erro ao converter a variavel para Boleano.", bootStrapMessageType.TYPE_DANGER);
            }
        }

        public static double Double(object objeto)
        {
            try
            {
                if (Convert.IsDBNull(objeto))
                    return 0;

                return Convert.ToDouble(objeto);
            }
            catch (Exception)
            {
                throw new CustomException("Ocorreu um erro ao converter a variavel para double.", bootStrapMessageType.TYPE_DANGER);
            }
        }

        public static double? DoubleNull(object objeto)
        {
            try
            {
                if (Convert.IsDBNull(objeto))
                    return null;

                return Convert.ToDouble(objeto);
            }
            catch (Exception)
            {
                throw new CustomException("Ocorreu um erro ao converter a variavel para double.", bootStrapMessageType.TYPE_DANGER);
            }
        }

        public static string String(object objeto)
        {
            try
            {
                if (Convert.IsDBNull(objeto))
                    return string.Empty;

                return objeto.ToString();
            }
            catch (Exception)
            {
                throw new CustomException("Ocorreu um erro ao converter a variavel para string.", bootStrapMessageType.TYPE_DANGER);
            }
        }

        public static char Char(object objeto)
        {
            try
            {
                if (Convert.IsDBNull(objeto))
                    return char.MinValue;

                return char.Parse(objeto.ToString());
            }
            catch (Exception)
            {
                throw new CustomException("Ocorreu um erro ao converter a variavel para char.", bootStrapMessageType.TYPE_DANGER);
            }
        }

        public static char? CharNull(object objeto)
        {
            try
            {
                if (Convert.IsDBNull(objeto))
                    return null;

                return char.Parse(objeto.ToString());
            }
            catch (Exception)
            {
                throw new CustomException("Ocorreu um erro ao converter a variavel para char.", bootStrapMessageType.TYPE_DANGER);
            }
        }

        public static TimeSpan Time(object objeto)
        {
            try
            {
                if (Convert.IsDBNull(objeto))
                    return TimeSpan.MinValue;

                return TimeSpan.Parse(objeto.ToString());
            }
            catch (Exception)
            {
                throw new CustomException("Ocorreu um erro ao converter a variavel para time.", bootStrapMessageType.TYPE_DANGER);
            }
        }

        public static TimeSpan? TimeNull(object objeto)
        {
            try
            {
                if (Convert.IsDBNull(objeto))
                    return null;

                return TimeSpan.Parse(objeto.ToString());
            }
            catch (Exception)
            {
                throw new CustomException("Ocorreu um erro ao converter o time para nulo.", bootStrapMessageType.TYPE_DANGER);
            }
        }

        #endregion 


        public static DataTable ToDataTable<T>(IEnumerable<T> dados)
        {
            DataTable dt = new DataTable();


            PropertyInfo[] columns = null;

            if (dados == null) return dt;

            foreach (T Record in dados)
            {

                if (columns == null)
                {
                    columns = ((Type)Record.GetType()).GetProperties();
                    foreach (PropertyInfo GetProperty in columns)
                    {
                        Type colType = GetProperty.PropertyType;

                        if ((colType.IsGenericType) && (colType.GetGenericTypeDefinition()
                        == typeof(Nullable<>)))
                        {
                            colType = colType.GetGenericArguments()[0];
                        }

                        dt.Columns.Add(new DataColumn(GetProperty.Name, colType));
                    }
                }

                DataRow dr = dt.NewRow();

                foreach (PropertyInfo pinfo in columns)
                {
                    dr[pinfo.Name] = pinfo.GetValue(Record, null) == null ? DBNull.Value : pinfo.GetValue
                    (Record, null);
                }

                dt.Rows.Add(dr);
            }
            return dt;
        } 
    }
}
