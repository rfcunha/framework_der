﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;

namespace Conexao.EntityFramework.Icontract
{
    public interface  IContract<T,X> : IDisposable
        where T : class, new()
        where X : DbContext, new()
    {
              T INSERT(T obj);
              bool UPDATE(T obj);
              bool UPDATE(T obj, params Expression<Func<T, object>>[] properties);
              bool DELETE(T obj);
              T FIND(int id);
              IQueryable<T> FIND(Expression<Func<T, bool>> Where = null);  
    }


    public interface IContract<T> : IDisposable
        where T : class, new()
    {
        T INSERT(T obj);
        bool UPDATE(T obj);
        bool UPDATE(T obj, params Expression<Func<T, object>>[] properties);
        bool DELETE(T obj);
        T FIND(int id);
        IQueryable<T> FIND(Expression<Func<T, bool>> Where = null);
    }
}
 