﻿using System.Configuration;
using System.Data.Entity;
using Suporte.Criptografia;

namespace Conexao.EntityFramework
{
    public class BaseContext<T> : DbContext where T : DbContext
    { 
        static BaseContext()
        {
            Database.SetInitializer<T>(null);
        }
        protected BaseContext(string conexao)
            : base(conexao)
        { 
        }
    } 
}
