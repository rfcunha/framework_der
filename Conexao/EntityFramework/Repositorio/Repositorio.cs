﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;
using System.Linq.Expressions;
using Conexao.EntityFramework.Icontract;

namespace Conexao.EntityFramework.Repositorio
{
    /// <summary>
    /// CLASSE DE CRUD DO PARA ENTITY FRAMEWORK
    /// </summary>
    /// <typeparam name="TClass"></typeparam>
    /// <typeparam name="TCtx"></typeparam>
    public class Repositorio<TClass, TCtx> : IContract<TClass, TCtx>where TClass : class, new()where TCtx : DbContext, new()
    {
        public TCtx _ctxContext { get; set; }

        public Repositorio()
        {
            if (_ctxContext == null)
            {
                _ctxContext = new TCtx();
            }

            _ctxContext.Configuration.AutoDetectChangesEnabled = false;
            _ctxContext.Configuration.EnsureTransactionsForFunctionsAndCommands = false;
            _ctxContext.Configuration.LazyLoadingEnabled = false;
            _ctxContext.Configuration.ProxyCreationEnabled = false;
            _ctxContext.Configuration.UseDatabaseNullSemantics = false;
            _ctxContext.Configuration.ValidateOnSaveEnabled = false;
        }

        //protected void CloseConnection(bool statusConexao = false)
        //{
        //    Dispose(statusConexao);
        //}

        public void Dispose()
        {
            Dispose(true);
        }

        // Protected implementation of Dispose pattern.
        protected virtual void Dispose(bool disposing)
        {
            if (!disposing)
                return;

            if (disposing)
            {
                if (_ctxContext == null) return;
                _ctxContext.Dispose();
                GC.SuppressFinalize(this);
            }
        }

        /// <summary>
        /// EXECUTA METODO T-SQL
        /// </summary>
        /// <param name="script">NOME DO PROCEDIMENTO</param>
        /// <param name="parameters">Parametros Sql</param>
        /// <returns>Retorna Booleano, se Executado com Sucesso ou Erro</returns>
        //public bool EXECUTE_SCRIPT(string script, SqlParameter[] parameters);
        //public bool EXECUTE_SCRIPT(string script, object[] parameters)
        //{
        //    try
        //    {
        //        var objCtx = ((IObjectContextAdapter)_ctxContext).ObjectContext;

        //        objCtx.ExecuteStoreCommand(script, parameters);

        //        return true;
        //    }
        //    catch (CustomException ce)
        //    {
        //        throw new CustomException(ce.Message, MessageBox.MessageType.error);
        //    }
        //    catch (Exception)
        //    {
        //        throw new CustomException("Ocorreu um erro desconhecido no sistema.</br>Entre em contato com o suporte.", MessageBox.MessageType.error);
        //    }
        //}

        /// <summary>
        /// EXECUTA METODO DE INSERIR OBJETO ENTITY
        /// </summary>
        /// <param name="obj">Tipo do Objeto</param> 
        /// <returns>Retorna => 1 - sucesso ou 0 - erro</returns>
        //public int INSERT(T obj);
        public virtual TClass INSERT(TClass obj)
        {

            _ctxContext.Configuration.ValidateOnSaveEnabled = false;
            _ctxContext.Configuration.AutoDetectChangesEnabled = true;

            _ctxContext.Set<TClass>().Add(obj);

            //_ctxContext.SaveChanges();

            //var objectStateEntry = ((IObjectContextAdapter)this).ObjectContext.ObjectStateManager.GetObjectStateEntry(obj);

            //return (int) objectStateEntry.EntityKey.EntityKeyValues[0].Value;

            return obj;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        public virtual bool INSERT(List<TClass> list)
        {

            _ctxContext.Configuration.ValidateOnSaveEnabled = false;
            _ctxContext.Configuration.AutoDetectChangesEnabled = true;

            _ctxContext.Set<TClass>().AddRange(list);

            //_ctxContext.SaveChanges();

            //var objectStateEntry = ((IObjectContextAdapter)this).ObjectContext.ObjectStateManager.GetObjectStateEntry(obj);

            //return (int) objectStateEntry.EntityKey.EntityKeyValues[0].Value;

            return true;
        }

        /// <summary>
        /// EXECUTA METODO DE UPDATE OBJETO ENTITY
        /// </summary>
        /// <param name="obj">Tipo do Objeto</param> 
        /// <returns>Retorna => true - sucesso ou false - erro</returns>
        //public bool UPDATE(T obj);
        public virtual bool UPDATE(TClass obj)
        {
            _ctxContext.Configuration.ValidateOnSaveEnabled = false;
            _ctxContext.Configuration.AutoDetectChangesEnabled = true;
            _ctxContext.Entry(obj).State = EntityState.Modified;

            //_ctxContext.SaveChanges();

            return true;
        }

        /// <summary>
        /// EXECUTA METODO DE UPDATE OBJETO ENTITY EM ELEMENTOS ESPECIFICOS
        /// </summary>
        /// <param name="obj">Tipo do Objeto</param>  
        /// <param name="properties">Propriedades</param> 
        /// <returns>Retorna => true - sucesso ou false - erro</returns>
        //public bool UPDATE(T obj, params Expression<Func<T, object>>[] properties);
        public virtual bool UPDATE(TClass obj, params Expression<Func<TClass, object>>[] properties)
        {
            _ctxContext.Configuration.ValidateOnSaveEnabled = false;
            _ctxContext.Configuration.AutoDetectChangesEnabled = true;
            _ctxContext.Entry(obj).State = EntityState.Modified;
            _ctxContext.Set<TClass>().Attach(obj);

            foreach (var p in properties)
            {
                _ctxContext.Entry(obj).Property(p).IsModified = true;
            }

            //_ctxContext.SaveChanges();

            return true;
        }

        /// <summary>
        /// EXECUTA METODO DE DELETE OBJETO ENTITY
        /// </summary>
        /// <param name="obj">Tipo do Objeto</param> 
        /// <returns>Retorna => true - sucesso ou false - erro</returns>
        //public bool DELETE(T obj);
        public virtual bool DELETE(TClass obj)
        {
            _ctxContext.Configuration.ValidateOnSaveEnabled = false;
            _ctxContext.Set<TClass>().Attach(obj);
            _ctxContext.Set<TClass>().Remove(obj);

            //_ctxContext.SaveChanges();

            return true;
        }

        /// <summary>
        /// EXECUTA METODO DE GET_BY OBJETO ENTITY
        /// </summary>
        /// <param name="id">KEY do Objeto</param> 
        /// <returns>Retorna o OBJETO</returns>
        //public T GET_BY(int id);
        public TClass FIND(int id)
        {
            var obj = _ctxContext.Set<TClass>().Find(id);
            if (obj != null)
            {
                _ctxContext.Entry(obj).State = EntityState.Detached;
            }

            return obj;
        }

        /// <summary>
        /// EXECUTA METODO DE GET_ALL OBJETO ENTITY TRAZENDO UMA IQUERYABLE DO TIPO T
        /// </summary>
        /// <param name="Where">Condições especificas para trazer o objeto</param> 
        /// <returns>Retorna o IQueryable do Tipo</returns>
        //public IQueryable<T> GET_ALL(Expression<Func<T, bool>> Where = null));
        public virtual IQueryable<TClass> FIND(Expression<Func<TClass, bool>> Where = null)
        {
            _ctxContext.Configuration.AutoDetectChangesEnabled = false;
            _ctxContext.Configuration.ProxyCreationEnabled = false;
            _ctxContext.Configuration.UseDatabaseNullSemantics = false;
            _ctxContext.Configuration.ValidateOnSaveEnabled = false;
            _ctxContext.Configuration.LazyLoadingEnabled = false;

            if (Where != null)
            {
                return _ctxContext.Set<TClass>().Where(Where).AsNoTracking();
            }

            return _ctxContext.Set<TClass>();
        }



        /// <summary>
        /// EXECUTA METODO DE GET_ALL OBJETO ENTITY TRAZENDO UMA IQUERYABLE DO TIPO T
        /// </summary>
        /// <param name="Where">Condições especificas para trazer o objeto</param> 
        /// <returns>Retorna o IQueryable do Tipo</returns>
        //public IQueryable<T> GET_ALL(Expression<Func<T, bool>> Where = null));
        public virtual IQueryable<TClass> FIND<TClass>(Expression<Func<TClass, bool>> Where = null) where TClass : class,new()
        {
            _ctxContext.Configuration.AutoDetectChangesEnabled = false;
            _ctxContext.Configuration.ProxyCreationEnabled = false;
            _ctxContext.Configuration.UseDatabaseNullSemantics = false;
            _ctxContext.Configuration.ValidateOnSaveEnabled = false;
            _ctxContext.Configuration.LazyLoadingEnabled = false;

            if (Where != null)
            {
                return _ctxContext.Set<TClass>().Where(Where).AsNoTracking();
            }

            return _ctxContext.Set<TClass>();
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="Where"></param>
        /// <returns></returns>
        //public virtual bool VALIDATE(Expression<Func<TClass, bool>> Where)
        //{
        //    _ctxContext.Configuration.AutoDetectChangesEnabled = false;
        //    _ctxContext.Configuration.ProxyCreationEnabled = false;
        //    _ctxContext.Configuration.UseDatabaseNullSemantics = false;
        //    _ctxContext.Configuration.ValidateOnSaveEnabled = false;
        //    _ctxContext.Configuration.LazyLoadingEnabled = false;

        //    return _ctxContext.Set<TClass>().Any(Where);
        //}

        public virtual void SaveChanges()
        {
            try
            {
                _ctxContext.SaveChanges();
            }
            catch (DbEntityValidationException ex)
            {
                throw;
            }
        }

        //private void ConfigurarEntity(bool status)
        //{
        //    _ctxContext.Configuration.AutoDetectChangesEnabled = status;
        //    _ctxContext.Configuration.ProxyCreationEnabled = status;
        //    _ctxContext.Configuration.UseDatabaseNullSemantics = status;
        //    _ctxContext.Configuration.ValidateOnSaveEnabled = status;
        //    _ctxContext.Configuration.LazyLoadingEnabled = false;
        //}
    }
}
