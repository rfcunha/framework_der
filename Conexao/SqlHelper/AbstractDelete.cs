﻿using System;
using System.Data;
using System.Data.SqlClient;
using Suporte.Exceptions;
using Suporte.MsgBox;

namespace Conexao.SqlHelper
{
    public abstract class AbstractDelete<T> : AbstractUpdate<T> where T : new()
    {
        public abstract string getStoredProcedureDelete();
        //public abstract SqlParameter[] getSqlParameterDelete(T obj);
        public abstract SqlParameter[] getSqlParameterDelete(int obj);

        /// <summary>
        /// EXECUTA O METODO QUE DELETA 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public virtual bool DELETE(int id)
        {
            try
            {
                new SqlHelper().ExecuteNonQuery(getConnection(), CommandType.StoredProcedure, getStoredProcedureDelete(), getSqlParameterDelete(id));
                return true;
            }
            catch (SqlException ex)
            {
                switch (ex.Number)
                {
                    case 50000:
                        throw new CustomException(ex.Message, bootStrapMessageType.TYPE_WARNING);
                    default:
                        throw new CustomException(ex.Message, bootStrapMessageType.TYPE_DANGER);
                }
            }
            catch (CustomException ce)
            {
                throw new CustomException(ce.Message, bootStrapMessageType.TYPE_DANGER);
            }
            catch (Exception)
            {
                throw new CustomException("Ocorreu um erro desconhecido no sistema.</br>Entre em contato com o suporte.", bootStrapMessageType.TYPE_DANGER);
            }
        }
    }
}
