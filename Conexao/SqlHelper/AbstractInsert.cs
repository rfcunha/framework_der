﻿using System;
using System.Data;
using System.Data.SqlClient;
using Suporte.Exceptions;
using Suporte.MsgBox;

namespace Conexao.SqlHelper
{
    public abstract class AbstractInsert<T> : AbstractRead<T> where T : new()
    {
        public abstract string getStoredProcedureInsert();
        public abstract SqlParameter[] getSqlParameterInsert(T obj);

        /// <summary>
        /// EXECUTA O METODO QUE INSERE O OBJETO
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public virtual int INSERT(T obj)
        {
            try
            {
                int id = Convert.ToInt32(new SqlHelper().ExecuteScalar(getConnection(), CommandType.StoredProcedure, getStoredProcedureInsert(), getSqlParameterInsert(obj)));
                return id;
            }
            catch (ConstraintException ex)
            {
                throw new CustomException(ex.Message, bootStrapMessageType.TYPE_DANGER);
            }
            catch (SqlException ex)
            {
                switch (ex.Number)
                {
                    case 50000:
                        throw new CustomException(ex.Message, bootStrapMessageType.TYPE_WARNING);
                    default:
                        throw new CustomException(ex.Message, bootStrapMessageType.TYPE_DANGER);
                }
            }
            catch (CustomException ce)
            {
                throw new CustomException(ce.Message, bootStrapMessageType.TYPE_DANGER);
            }
            catch (Exception)
            {
                throw new CustomException("Ocorreu um erro desconhecido no sistema.</br>Entre em contato com o suporte.", bootStrapMessageType.TYPE_DANGER);
            }
        }

    }
}
