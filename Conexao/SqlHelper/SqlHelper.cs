﻿using System;
using System.Data;
using System.Data.SqlClient;

namespace Conexao.SqlHelper
{
    public class SqlHelper
    {
        protected SqlConnection conn = null;
        //protected SqlCommand command = null;


        /// <summary>
        /// Abre a conexão com o banco de dados
        /// </summary>
        protected void OpenConnection(string strConn)
        {
            if (conn == null || (conn != null && conn.State != ConnectionState.Open))
            {
                conn = new SqlConnection(strConn);
                conn.Open();
            }
        }

        #region ExecuteNonQuery

        public void ExecuteNonQuery(string conectionString, CommandType commandType, string comandText)
        {
            try
            {
                using (var conn = new SqlConnection(conectionString))
                {
                    using (var command = new SqlCommand())
                    {
                        conn.Open();

                        command.Parameters.Clear();
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = comandText;
                        command.Connection = conn;

                        command.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void ExecuteNonQuery(string conectionString, CommandType commandType, string comandText, params SqlParameter[] commandParameters)
        {
            try
            {
                using (var conn = new SqlConnection(conectionString))
                {
                    using (var command = new SqlCommand())
                    {
                        conn.Open();

                        command.Parameters.Clear();
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = comandText;
                        command.Parameters.AddRange(commandParameters);
                        command.Connection = conn;

                        command.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        #endregion

        #region ExecuteReader

        public IDataReader ExecuteReader(string conectionString, CommandType commandType, string comandText)
        {
            try
            {
                //using (var conn = new SqlConnection(conectionString))
                //{
                using (var command = new SqlCommand())
                {
                    OpenConnection(conectionString);

                    command.Parameters.Clear();
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = comandText;
                    command.Connection = conn;

                    return command.ExecuteReader(CommandBehavior.CloseConnection);
                }
                //}
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IDataReader ExecuteReader(string conectionString, CommandType commandType, string comandText, params SqlParameter[] commandParameters)
        {
            try
            {
                //using (var conn = new SqlConnection(conectionString))
                //{
                using (var command = new SqlCommand())
                {
                    OpenConnection(conectionString);

                    command.Parameters.Clear();
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = comandText;
                    command.Parameters.AddRange(commandParameters);
                    command.Connection = conn;

                    return command.ExecuteReader(CommandBehavior.CloseConnection);
                }
                //}
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        

        #endregion

        #region ExecuteDataset

        public DataSet ExecuteDataset(string conectionString, CommandType commandType, string comandText, params SqlParameter[] commandParameters)
        {
            try
            {
                using (var conn = new SqlConnection(conectionString))
                {
                    using (var command = new SqlCommand())
                    {
                        conn.Open();

                        command.Parameters.Clear();
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = comandText;
                        command.Parameters.AddRange(commandParameters);
                        command.Connection = conn;

                        using (var dataAdapter = new SqlDataAdapter())
                        {
                            var ds = new DataSet();

                            dataAdapter.SelectCommand = command;

                            dataAdapter.Fill(ds);

                            return ds;
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                throw ex;
            }
        }

        public DataSet ExecuteDataset(string conectionString, CommandType commandType, string comandText)
        {
            try
            {
                using (var conn = new SqlConnection(conectionString))
                {
                    using (var command = new SqlCommand())
                    {
                        conn.Open();

                        command.Parameters.Clear();
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = comandText;
                        command.Connection = conn;

                        using (var dataAdapter = new SqlDataAdapter())
                        {
                            var ds = new DataSet();

                            dataAdapter.SelectCommand = command;

                            dataAdapter.Fill(ds);

                            return ds;
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                throw ex;
            }
        }

        #endregion

        #region ExecuteDataTable

        public DataTable ExecuteDataTable(string conectionString, string comandText, params SqlParameter[] commandParameters)
        {
            try
            {
                using (var conn = new SqlConnection(conectionString))
                {
                    using (var command = new SqlCommand())
                    {
                        conn.Open();

                        command.Parameters.Clear();
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = comandText;
                        command.Connection = conn;

                        using (var dataAdapter = new SqlDataAdapter())
                        {
                            var ds = new DataSet();

                            dataAdapter.SelectCommand = command;

                            dataAdapter.Fill(ds);

                            return ds.Tables[0];
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                throw ex;
            }
        }

        #endregion

        #region ExecuteScalar

        public object ExecuteScalar(string conectionString, CommandType commandType, string comandText, params SqlParameter[] commandParameters)
        {
            try
            {
                using (var conn = new SqlConnection(conectionString))
                {
                    using (var command = new SqlCommand())
                    {
                        conn.Open();

                        command.Parameters.Clear();
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = comandText;
                        command.Parameters.AddRange(commandParameters);
                        command.Connection = conn;

                        object result = command.ExecuteScalar();

                        return result != null ? int.Parse(result.ToString()) : 0;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public object ExecuteScalar(string conectionString, CommandType commandType, string comandText)
        {
            try
            {
                using (var conn = new SqlConnection(conectionString))
                {
                    using (var command = new SqlCommand())
                    {
                        conn.Open();

                        command.Parameters.Clear();
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = comandText;
                        command.Connection = conn;

                        object result = command.ExecuteScalar();

                        return result != null ? int.Parse(result.ToString()) : 0;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion
    }
}
