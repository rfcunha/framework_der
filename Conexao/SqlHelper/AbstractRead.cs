﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Suporte.Exceptions;
using Suporte.MsgBox;

namespace Conexao.SqlHelper
{
    public abstract class AbstractRead<T> where T : new()
    {
        public abstract string getConnection();
        public abstract string getStoredProdedureGet_All();
        public abstract string getStoredProdedureGet_By();
        public abstract SqlParameter[] getSqlParameterGet_All(T obj);
        //public abstract SqlParameter[] getSqlParameterGet_By(T obj);
        public abstract SqlParameter[] getSqlParameterGet_By(int id);

        /// <summary>
        /// CONVERT O DATAROW EM OBJETO
        /// </summary>
        /// <param name="dr"></param>
        /// <returns></returns>
        public abstract T ToObject(IDataReader dr);

        //public abstract DataRow ToDataSet(object obj);

        /// <summary>
        /// METODO QUE RETORNA UMA LISTA DE OBJETOS
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public virtual List<T> GET_ALL(T obj)
        {
            var tmpList = new List<T>();

            try
            {
                using (IDataReader reader = new SqlHelper().ExecuteReader(getConnection(), CommandType.StoredProcedure, getStoredProdedureGet_All(), getSqlParameterGet_All(obj)))
                {
                    while (reader.Read())
                    {
                        tmpList.Add(ToObject(reader));
                    }

                    reader.Close();
                    reader.Dispose();

                    return tmpList;
                }
            }
            catch (SqlException ex)
            {
                switch (ex.Number)
                {
                    case 50000:
                        throw new CustomException(ex.Message, bootStrapMessageType.TYPE_WARNING);
                    default:
                        throw new CustomException(ex.Message, bootStrapMessageType.TYPE_DANGER);
                }
            }
            catch (CustomException ce)
            {
                throw new CustomException(ce.Message, bootStrapMessageType.TYPE_DANGER);
            }
            catch (Exception ex)
            {
                throw new CustomException("Ocorreu um erro desconhecido no sistema.</br>Entre em contato com o suporte.", bootStrapMessageType.TYPE_DANGER);
            }
        }


        /// <summary>
        /// METODO QUE RETORNA O OBJETO
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        //public virtual T GET_BY(T obj)
        //{
        //    object tmp_result = obj;

        //    try
        //    {
        //        using (var reader = SqlHelper.GetInstance().ExecuteDataTable(getConnection(), getStoredProdedureGet_By(), getSqlParameterGet_By(obj)))
        //        {
        //            foreach (DataRow dr in reader.Rows)
        //            {
        //                obj = ToObject(dr);
        //            }

        //            return obj;
        //        }
        //    }
        //    catch (SqlException ex)
        //    {
        //        switch (ex.Number)
        //        {
        //            case 50000:
        //                throw new CustomException(ex.Message, MessageType.alert);
        //                break;

        //            default:
        //                throw new CustomException(ex.Message, MessageType.error);
        //                break;
        //        }
        //    }
        //    catch (Exception)
        //    {
        //        throw new CustomException("Ocorreu um erro desconhecido no sistema.</br>Entre em contato com o suporte.", MessageType.error);
        //    }
        //}

        public virtual T GET_BY(int id)
        {
            //var obj = new object();

            var obj = new T();

            try
            {
                using (IDataReader reader = new SqlHelper().ExecuteReader(getConnection(), CommandType.StoredProcedure, getStoredProdedureGet_By(), getSqlParameterGet_By(id)))
                {
                    while (reader.Read())
                    {
                        obj = ToObject(reader);
                    }

                    //return (T)obj;

                    return obj;
                }
            }
            catch (SqlException ex)
            {
                switch (ex.Number)
                {
                    case 50000:
                        throw new CustomException(ex.Message, bootStrapMessageType.TYPE_WARNING);
                    default:
                        throw new CustomException(ex.Message, bootStrapMessageType.TYPE_DANGER);
                }
            }
            catch (CustomException ce)
            {
                throw new CustomException(ce.Message, bootStrapMessageType.TYPE_DANGER);
            }
            catch (Exception)
            {
                throw new CustomException("Ocorreu um erro desconhecido no sistema.</br>Entre em contato com o suporte.", bootStrapMessageType.TYPE_DANGER);
            }
        }

    }
}
