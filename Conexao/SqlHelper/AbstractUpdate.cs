﻿using System;
using System.Data;
using System.Data.SqlClient;
using Suporte.Exceptions;
using Suporte.MsgBox;


namespace Conexao.SqlHelper
{
    public abstract class AbstractUpdate<T> : AbstractInsert<T> where T : new()
    {
        public abstract string getStoredProdedureUpdate();
        public abstract string getStoredProdedureStatus();
        public abstract SqlParameter[] getSqlParameterUpdate(T obj);
        //public abstract SqlParameter[] getSqlParameterStatus(T obj);
        public abstract SqlParameter[] getSqlParameterStatus(int id, bool status);

        /// <summary>
        /// EXECUTA O METODO QUE ALTERA O OBJETO
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public virtual bool UPDATE(T obj)
        {
            try
            {
                new SqlHelper().ExecuteNonQuery(getConnection(), CommandType.StoredProcedure, getStoredProdedureUpdate(), getSqlParameterUpdate(obj));
                return true;
            }
            catch (SqlException ex)
            {
                switch (ex.Number)
                {
                    case 50000:
                        throw new CustomException(ex.Message, bootStrapMessageType.TYPE_WARNING);
                    default:
                        throw new CustomException(ex.Message, bootStrapMessageType.TYPE_DANGER);
                }
            }
            catch (CustomException ce)
            {
                throw new CustomException(ce.Message, bootStrapMessageType.TYPE_DANGER);
            }
            catch (Exception)
            {
                throw new CustomException("Ocorreu um erro desconhecido no sistema.</br>Entre em contato com o suporte.", bootStrapMessageType.TYPE_DANGER);
            }
        }

        /// <summary>
        /// EXECUTA O METODO QUE ALTERA O STATUS DO OBJETO
        /// </summary>
        /// <param name="id"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        public virtual bool STATUS(int id, bool status)
        {
            try
            {
                new SqlHelper().ExecuteNonQuery(getConnection(), CommandType.StoredProcedure, getStoredProdedureStatus(), getSqlParameterStatus(id, status));
                return true;
            }
            catch (SqlException ex)
            {
                switch (ex.Number)
                {
                    case 50000:
                        throw new CustomException(ex.Message, bootStrapMessageType.TYPE_WARNING);
                    default:
                        throw new CustomException(ex.Message, bootStrapMessageType.TYPE_DANGER);
                }
            }
            catch (CustomException ce)
            {
                throw new CustomException(ce.Message, bootStrapMessageType.TYPE_DANGER);
            }
            catch (Exception)
            {
                throw new CustomException("Ocorreu um erro desconhecido no sistema.</br>Entre em contato com o suporte.", bootStrapMessageType.TYPE_DANGER);
            }
        }
    }
}
