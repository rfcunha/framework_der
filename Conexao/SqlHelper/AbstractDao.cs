﻿namespace Conexao.SqlHelper
{
    public abstract class AbstractDao<T> : AbstractDelete<T> where T : new()
    {

        // R - CR - CRU - CRUD

        /// <summary>
        /// METODO QUE INSERE O OBJETO 
        /// </summary>
        /// <param name="object"></param>
        /// <returns></returns>
        //public abstract object INSERT(object t);        

        ///// <summary>
        ///// METODO QUE ATUALIZA O OBJETO 
        ///// </summary>
        ///// <param name="object"></param>
        ///// <returns></returns>
        //public abstract bool UPDATE(object t);

        /// <summary>
        /// EXECUTA O METODO QUE ATIVA DE DESATIVA
        /// </summary>
        /// <param name="object"></param>
        /// <returns></returns>
        //public abstract bool STATUS(object t);

        /// <summary>
        /// EXECUTA O METODO QUE RETORNA O OBJETO
        /// </summary>
        /// <param name="object"></param>
        /// <returns></returns>
        //public abstract object GET_BY(object t);
 
        /// <summary>
        /// EXECUTA O METODO QUE RETORNA UMA LISTA DE OBJETOS
        /// </summary>
        /// <returns></returns>
        //public abstract List<object> GET_ALL(object t);
    }
}
