﻿using System.Collections.Generic;
using System.Data.Common;

namespace Conexao.AdoProvider.IContract
{
    /// <summary>
    /// Contrato para se colocar na classe DAO
    /// </summary>
    /// <typeparam name="T">CLASSE CRUD</typeparam> 
    public interface IContract<T> where T : class, new()
    {
        int INSERT(T obj);
        int UPDATE(T obj);
        bool STATUS(int id, bool status, string user_log);
        int DELETE(int id, string user_log);
        T GET_BY(int id);
        ICollection<T> GET_ALL(T obj);
        T ToObject(DbDataReader dataReader);
    }
}
