﻿using System;
using System.Data;
using System.Data.Common;

namespace Conexao.AdoProvider.Repositorio
{
    public class DbBase : IDisposable
    {
        /// <summary>
        ///  Váriaveis para criar um provider do banco de dados usados (classe de banco genérica)
        /// </summary>
        private string connectionString { get; set; }
        private readonly DbProviderFactory _factory;
        private readonly DbConnection _cn;
        private DbCommand _cm;
        private DbTransaction _sqlTrans;
        private DbDataAdapter _da;

        /// <summary>
        ///  Propriedades a serem usadas para o banco de dados
        /// </summary>
        public string CommandText { get; set; }
        public CommandType CommandType { get; set; }

        /// <summary>
        ///  Construtor  padrão
        /// </summary>
        /// <param name="connectionString"> String de conexão</param>
        /// <param name="providerName">Nome do provider do banco de dados</param>
        /// <returns>Retorna um objeto de conexão aberto</returns>
        public DbBase(string connectionString, string providerName = "System.Data.SqlClient")
        {
            try
            {
                this.connectionString = connectionString;
                _factory = DbProviderFactories.GetFactory(providerName);
                _cn = _factory.CreateConnection();
                _cn.ConnectionString = this.connectionString;
                CommandType = CommandType.StoredProcedure;
                _cm = _factory.CreateCommand();
                _da = _factory.CreateDataAdapter();

                ClearParameter();
                
                if (_cm != null) _cm.Connection = _cn;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void OpenConnection(bool isTransaction)
        {
            if (isTransaction)
            {
                _cn.Open();
                if (_sqlTrans == null)
                {
                    _cm.Transaction = _cm.Connection.BeginTransaction();
                    _sqlTrans = _cm.Transaction;
                }
                else
                {
                    _cm.Connection = _sqlTrans.Connection;
                    _cm.Transaction = _sqlTrans;
                }
            }
            else
            {
                _cn.Open();
            }
        }


        /// <summary>
        ///  ExecuteScalar
        /// </summary>
        /// <returns>Executa o metodo ExecuteReader onde o Objeto retornado deve ser tratado</returns>
        public object ExecuteScalar(bool isTransaction = false)
        {
            try
            {
                _cm.CommandType = CommandType;
                _cm.CommandText = CommandText;
                OpenConnection(isTransaction);

                if(isTransaction)
                    _sqlTrans.Commit();
                
                return _cm.ExecuteScalar();
            }
            catch (Exception ex)
            {
                if (isTransaction)
                    _sqlTrans.Rollback();

                throw ex;
            }
            finally
            {
                Dispose();
            }
        }

        /// <summary>
        ///  ExecuteNonQuery
        /// </summary>
        /// <returns>Retorna o numero de linhas afetadas / numero retornado</returns>
        public int ExecuteNonQuery(bool isTransaction = false)
        {
            try
            {
                _cm.CommandType = CommandType;
                _cm.CommandText = CommandText;
                OpenConnection(isTransaction);

                if (isTransaction)
                    _sqlTrans.Commit();

                return _cm.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                if (isTransaction)
                    _sqlTrans.Rollback();

                throw ex;
            }
            finally
            {
                Dispose();
            }
        }

        /// <summary>
        ///  ExecuteReader
        /// </summary>
        /// <returns>Executa o metodo ExecuteReader onde o DataReader vai aberto assim tendo que fechado na camada DAO</returns>
        public DbDataReader ExecuteReader()
        {
            try
            {
                _cm.CommandType = CommandType;
                _cm.CommandText = CommandText;
                OpenConnection(false);
                return _cm.ExecuteReader(CommandBehavior.CloseConnection);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        ///  ExecuteDataSet
        /// </summary>
        /// <returns>Executa o metodo ExecuteDataSet onde é retornado um DBDataAdapter</returns>
        public DataSet ExecuteDataSet()
        {
            try
            {
                _cm.CommandType = CommandType;
                _cm.CommandText = CommandText;
                _da.SelectCommand = _cm;
                var dataSet = new DataSet();
                _da.Fill(dataSet, "dataSet");
                return dataSet;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                Dispose();
            }
        }

        /// <summary>
        ///  Adicionar parametros
        /// </summary>
        /// <param name="parameterName">Nome do parametro</param>
        /// <param name="value"> valor do parametron</param>
        /// <param name="direction"> direçao do parametro</param>
        /// <returns>Adiciona um parametro para o evento a ser realizado (GRUD)</returns>
        public void SetParameter(string parameterName, object value, ParameterDirection direction = ParameterDirection.Input)
        {
            try
            {
                var parameter = _factory.CreateParameter();
                if (parameter == null) return;
                
                parameter.ParameterName = parameterName;
                parameter.Value = value;
                parameter.Direction = direction;
                _cm.Parameters.Add(parameter);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        ///  Adicionar parametros
        /// </summary>
        /// <param name="parameterName">Nome do parametro</param>
        /// <param name="value"> valor do parametron</param>
        /// <param name="type"> Tipo do valor (Int32, Int62, String, Data, DateTime2...) do parametro value</param>
        /// <param name="direction"> direçao do parametro</param>
        /// <returns>Adiciona um parametro para o evento a ser realizado (GRUD)</returns>
        public void SetParameter(string parameterName, object value, DbType type, ParameterDirection direction = ParameterDirection.Input)
        {
            try
            {
                var parameter = _factory.CreateParameter();
                if (parameter == null) return;
                parameter.ParameterName = parameterName;
                parameter.Value = value;
                parameter.DbType = type;
                parameter.Direction = direction;
                _cm.Parameters.Add(parameter);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        /// <summary>
        ///  Adicionar parametros
        /// </summary>
        /// <param name="parameterName">Nome do parametro</param>
        /// <param name="value"> valor do parametron</param>
        /// <param name="type"> Tipo do valor (Int32, Int62, String, Data, DateTime2...) do parametro value</param>
        /// <param name="direction"> direçao do parametro</param>
        /// <returns>Adiciona um parametro para o evento a ser realizado (GRUD)</returns>
        public DbParameter SetParameter(string parameterName, DbType type, ParameterDirection direction)
        {
            try
            {
                var parameter = _factory.CreateParameter();
                if (parameter == null)
                {
                    return null;
                }
                parameter.ParameterName = parameterName;
                parameter.DbType = type;
                parameter.Direction = direction;
                _cm.Parameters.Add(parameter);
                return parameter;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        /// <summary>
        /// Limpa parametros
        /// </summary>
        private void ClearParameter()
        {
            _cm.Parameters.Clear();
        }

        /// <summary>
        /// Dispose
        /// </summary>
        public void Dispose()
        {
            if (_cn == null) return;
            if (_cn.State == ConnectionState.Open)
            {
                _cn.Close();
            }
        }
    }
}
